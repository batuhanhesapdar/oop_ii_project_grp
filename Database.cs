﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data;


namespace oopproje
{
    /// <summary>
    /// Interact with database for all necessary functions and properties
    /// Implemented with singleton design pattern
    /// </summary>
    class Database
    {
        private SqlConnection connection = new SqlConnection();
        private static Database dbInstance;
        public Customer currentCustomer;
        private DataTable customerTable = new DataTable();
        private DataTable bookTable = new DataTable();
        private DataTable shoppingCartTable = new DataTable();
        private DataTable magazineTable = new DataTable();
        private DataTable musicTable = new DataTable();
        private DataTable billTable = new DataTable();
        private DataTable billDetailTable = new DataTable();

        private Database()
        {
            ConnectToDatabase();
        }

        public Customer CurrentCustomer
        {
            get { return currentCustomer; }
        }

        public DataTable BookTable
        {
            get { return bookTable; }
        }

        public DataTable ShoppingCartTable
        {
            get { return shoppingCartTable; }
        }

        public DataTable MagazinesTable
        {
            get { return magazineTable; }
        }

        public DataTable BillTable
        {
            get { return billTable; }
        }

        public DataTable MusicCDTable
        {
            get { return musicTable; }
        }

        public static Database getDbInstance
        {
            get
            {
                if (dbInstance == null)
                {
                    dbInstance = new Database();
                }
                return dbInstance;
            }

        }
        /// <summary>
        /// Assign the logined customer to current costumer property
        /// </summary>
        /// <param name="username">Logined customer username</param>
        public void SetCurrentCustomer(string username)
        {
            Logger logger = Logger.getInstance;
            logger.addLog("SetCurrentCustomer");

            SqlDataAdapter da = new SqlDataAdapter("Select * from Customers where Username='" + username + "'", connection);
            DataTable curUserTable = new DataTable();
            da.Fill(curUserTable);
            currentCustomer = new Customer(int.Parse(curUserTable.Rows[0]["Id"].ToString()), curUserTable.Rows[0]["Name"].ToString(), curUserTable.Rows[0]["Surname"].ToString(), curUserTable.Rows[0]["Address"].ToString(), curUserTable.Rows[0]["Email"].ToString(), curUserTable.Rows[0]["Username"].ToString(), curUserTable.Rows[0]["Password"].ToString());
        }
        /// <summary>
        /// General function to connect the database
        /// </summary>
        public void ConnectToDatabase()
        {
            Logger logger = Logger.getInstance;
            logger.addLog("ConnectToDatabase");

            try
            {
                String connectStr = "Data Source=SQL5097.site4now.net;Initial Catalog=db_a754a6_dbshopping;User Id=db_a754a6_dbshopping_admin;Password=asd123456";
                connection = new SqlConnection(connectStr);
                connection.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        /// <summary>
        /// Disconnect from the database
        /// </summary>
        public void CloseDatabaseConenction()
        {
            Logger logger = Logger.getInstance;
            logger.addLog("CloseDatabaseConenction");

            try
            {
                connection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        /// <summary>
        /// Checks the user from database </summary>
        /// <param name="username">Username of logined user</param>
        /// <param name="password">Password of logined user</param>
        /// <returns>A bool value that indicates the succes of login</returns>
        public bool LoginUser(string username, string password)
        {
            Logger logger = Logger.getInstance;
            logger.addLog("LoginUser");

            try
            {
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                command.CommandText = "Select * From Customers where Username='" + username + "'And Password='" + password + "'";
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    reader.Close();
                    return true;
                }
                else
                {
                    reader.Close();
                    return false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
        /// <summary>
        /// Save the user to the database customer table
        /// </summary>
        /// <param name="inputCustomer">Customer that is going to be registered</param>
        public void RegisterUser(Customer inputCustomer)
        {
            Logger logger = Logger.getInstance;
            logger.addLog("RegisterUser");

            try
            {
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                command.CommandText = "Select * From Customers where Username='" + inputCustomer.Username + "'Or Email='" + inputCustomer.Email + "'";
                SqlDataReader reader = command.ExecuteReader();

                if (!reader.Read())
                {
                    reader.Close();
                    SqlCommand commandForRegister = new SqlCommand("Insert into Customers(Name,Surname,Address,Email,Username,Password)values (@Name,@Surname,@Address,@Email,@Username,@Password)", connection);
                    commandForRegister.Parameters.AddWithValue("@Name", inputCustomer.Name);
                    commandForRegister.Parameters.AddWithValue("@Surname", inputCustomer.Surname);
                    commandForRegister.Parameters.AddWithValue("@Address", inputCustomer.Adress);
                    commandForRegister.Parameters.AddWithValue("@Email", inputCustomer.Email);
                    commandForRegister.Parameters.AddWithValue("@Username", inputCustomer.Username);
                    commandForRegister.Parameters.AddWithValue("@Password", inputCustomer.Password);
                    SqlDataReader readerForRegister = commandForRegister.ExecuteReader();
                    readerForRegister.Close();
                    MessageBox.Show("kayıt Başarılı", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    reader.Close();
                    MessageBox.Show("Bu email adresi veya username ile önceden kayıt olunmuş!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        /// <summary>
        /// Add books to a database table
        /// </summary>
        /// <param name="inputBook">Book that is added</param>
        public void AddBookToDatabase(Book inputCustomer)
        {
            Logger logger = Logger.getInstance;
            logger.addLog("AddBookToDatabase");

            try
            {

                SqlCommand commandForRegister = new SqlCommand("Insert into Books(Name,Price,ISBN,Author,Publisher,TotalPage,Stock)values (@Name,@Price,@ISBN,@Author,@Publisher,@TotalPage,@Stock)", connection);
                commandForRegister.Parameters.AddWithValue("@Name", inputCustomer.Name);
                commandForRegister.Parameters.AddWithValue("@Price", inputCustomer.Price);
                commandForRegister.Parameters.AddWithValue("@ISBN", inputCustomer.ISBN);
                commandForRegister.Parameters.AddWithValue("@Author", inputCustomer.Author);
                commandForRegister.Parameters.AddWithValue("@Publisher", inputCustomer.Publisher);
                commandForRegister.Parameters.AddWithValue("@TotalPage", inputCustomer.Page);
                commandForRegister.Parameters.AddWithValue("@Stock", inputCustomer.Stock);
                SqlDataReader readerForRegister = commandForRegister.ExecuteReader();
                readerForRegister.Close();
                MessageBox.Show("Ürün Eklendi", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        /// <summary>
        /// Add musics to a database table
        /// </summary>
        /// <param name="inputMusic">Music that is added</param>
        public void AddMusicToDatabase(MusicCD inputCustomer)
        {
            Logger logger = Logger.getInstance;
            logger.addLog("AddMusicToDatabase");

            try
            {
                SqlCommand commandForRegister = new SqlCommand("Insert into Musics(Name,Price,SingerName,AlbumName,MusicType,Stock)values (@Name,@Price,@SingerName,@AlbumName,@MusicType,@Stock)", connection);
                commandForRegister.Parameters.AddWithValue("@Name", inputCustomer.Name);
                commandForRegister.Parameters.AddWithValue("@Price", inputCustomer.Price);
                commandForRegister.Parameters.AddWithValue("@SingerName", inputCustomer.Singer);
                commandForRegister.Parameters.AddWithValue("@AlbumName", inputCustomer.AlbumName);
                commandForRegister.Parameters.AddWithValue("@MusicType", inputCustomer.Type.ToString());
                commandForRegister.Parameters.AddWithValue("@Stock", inputCustomer.Stock);
                SqlDataReader readerForRegister = commandForRegister.ExecuteReader();
                readerForRegister.Close();
                MessageBox.Show("Ürün Eklendi", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        /// <summary>
        /// Update musics from database table
        /// </summary>
        /// <param name="inputMusic">Music that is updated</param>
        public void UpdateMusics(MusicCD inputCustomer)
        {
            Logger logger = Logger.getInstance;
            logger.addLog("UpdateMusics");

            try
            {
                SqlCommand commandForRegister = new SqlCommand("Update Musics set Name = @Name, Price = @Price, SingerName = @SingerName, AlbumName = @AlbumName, MusicType = @MusicType, Stock = @Stock where ID = @ID", connection);
                commandForRegister.Parameters.AddWithValue("@ID", inputCustomer.Id);
                commandForRegister.Parameters.AddWithValue("@Name", inputCustomer.Name);
                commandForRegister.Parameters.AddWithValue("@Price", inputCustomer.Price);
                commandForRegister.Parameters.AddWithValue("@SingerName", inputCustomer.Singer);
                commandForRegister.Parameters.AddWithValue("@AlbumName", inputCustomer.AlbumName);
                commandForRegister.Parameters.AddWithValue("@MusicType", inputCustomer.Type.ToString());
                commandForRegister.Parameters.AddWithValue("@Stock", inputCustomer.Stock);
                SqlDataReader readerForRegister = commandForRegister.ExecuteReader();
                readerForRegister.Close();
                MessageBox.Show("Ürün Eklendi", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        /// <summary>
        /// Update magazines from database table
        /// </summary>
        /// <param name="inputMagazine">Magazine that is updated</param>
        public void UpdateMagazines(Magazine inputCustomer)
        {
            Logger logger = Logger.getInstance;
            logger.addLog("UpdateMagazines");

            try
            {
                SqlCommand commandForRegister = new SqlCommand("Update Magazines set Name = @Name, Price = @Price, Type = @Type, Issue = @Issue, Stock = @Stock where ID = @ID", connection);
                commandForRegister.Parameters.AddWithValue("@ID", inputCustomer.Id);
                commandForRegister.Parameters.AddWithValue("@Name", inputCustomer.Name);
                commandForRegister.Parameters.AddWithValue("@Price", inputCustomer.Price);
                commandForRegister.Parameters.AddWithValue("@Type", inputCustomer.Type.ToString());
                commandForRegister.Parameters.AddWithValue("@Issue", inputCustomer.Issue);
                commandForRegister.Parameters.AddWithValue("@Stock", inputCustomer.Stock);
                SqlDataReader readerForRegister = commandForRegister.ExecuteReader();
                readerForRegister.Close();
                MessageBox.Show("Ürün Eklendi", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        /// <summary>
        /// Update books from database table
        /// </summary>
        /// <param name="inputBook">Book that is updated</param>
        public void UpdateBooks(Book inputCustomer)
        {
            Logger logger = Logger.getInstance;
            logger.addLog("UpdateBooks");

            try
            {
                SqlCommand commandForRegister = new SqlCommand("Update Books set Name = @Name, Price = @Price, ISBN = @ISBN, Author = @Author, Publisher = @Publisher, TotalPage = @TotalPage, Stock = @Stock where ID = @ID", connection);
                commandForRegister.Parameters.AddWithValue("@ID", inputCustomer.Id);
                commandForRegister.Parameters.AddWithValue("@Name", inputCustomer.Name);
                commandForRegister.Parameters.AddWithValue("@Price", inputCustomer.Price);
                commandForRegister.Parameters.AddWithValue("@ISBN", inputCustomer.ISBN);
                commandForRegister.Parameters.AddWithValue("@Author", inputCustomer.Author);
                commandForRegister.Parameters.AddWithValue("@Publisher", inputCustomer.Publisher);
                commandForRegister.Parameters.AddWithValue("@TotalPage", inputCustomer.Page);
                commandForRegister.Parameters.AddWithValue("@Stock", inputCustomer.Stock);
                SqlDataReader readerForRegister = commandForRegister.ExecuteReader();
                readerForRegister.Close();
                MessageBox.Show("Ürün Eklendi", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        /// <summary>
        /// Add magazines to a database table
        /// </summary>
        /// <param name="inputMagazine">Magazine that is added</param>
        public void AddMagazineToDatabase(Magazine inputCustomer)
        {
            Logger logger = Logger.getInstance;
            logger.addLog("AddMagazineToDatabase");

            try
            {

                SqlCommand commandForRegister = new SqlCommand("Insert into Magazines(Name,Price,Type,Issue,Stock)values (@Name,@Price,@Type,@Issue,@Stock)", connection);
                commandForRegister.Parameters.AddWithValue("@Name", inputCustomer.Name);
                commandForRegister.Parameters.AddWithValue("@Price", inputCustomer.Price);
                commandForRegister.Parameters.AddWithValue("@Type", inputCustomer.Type.ToString());
                commandForRegister.Parameters.AddWithValue("@Issue", inputCustomer.Issue);
                commandForRegister.Parameters.AddWithValue("@Stock", inputCustomer.Stock);
                SqlDataReader readerForRegister = commandForRegister.ExecuteReader();
                readerForRegister.Close();
                MessageBox.Show("Ürün Eklendi", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        /// <summary>
        /// Update all the books inside relevant data table
        /// </summary>
        public void UpdateBookTable()
        {
            Logger logger = Logger.getInstance;
            logger.addLog("UpdateBookTable");

            try
            {
                bookTable.Clear();
                SqlDataAdapter dataAdapter = new SqlDataAdapter("Select * from Books", connection);
                dataAdapter.Fill(bookTable);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        /// <summary>
        /// Update all the bills inside relevant data table
        /// </summary>
        public void UpdateBillTable()
        {
            Logger logger = Logger.getInstance;
            logger.addLog("UpdateBillTable");

            try
            {
                billTable.Clear();
                SqlDataAdapter dataAdapter = new SqlDataAdapter("Select * from Bills where CustomerID='" + currentCustomer.Id + "'", connection);
                dataAdapter.Fill(billTable);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        /// <summary>
        /// Update all the magazines inside relevant data table
        /// </summary>
        public void UpdateMagazineTable()
        {
            Logger logger = Logger.getInstance;
            logger.addLog("UpdateMagazineTable");

            try
            {
                magazineTable.Clear();
                SqlDataAdapter dataAdapter = new SqlDataAdapter("Select * from Magazines", connection);
                dataAdapter.Fill(magazineTable);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        /// <summary>
        /// Update all the musics inside relevant data table
        /// </summary>
        public void UpdateMusicCDTable()
        {
            Logger logger = Logger.getInstance;
            logger.addLog("UpdateMusicCDTable");

            try
            {
                MusicCDTable.Clear();
                SqlDataAdapter dataAdapter = new SqlDataAdapter("Select * from Musics", connection);
                dataAdapter.Fill(musicTable);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        /// <summary>
        /// Update the shopping cart data table
        /// </summary>
        public void UpdateShoppingCartTable()
        {
            Logger logger = Logger.getInstance;
            logger.addLog("UpdateShoppingCartTable");

            try
            {
                shoppingCartTable.Clear();
                SqlDataAdapter dataAdapter = new SqlDataAdapter("Select * from ShoppingCart where CustomerID='" + currentCustomer.Id + "'", connection);
                dataAdapter.Fill(shoppingCartTable);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        /// <summary>
        /// Add a new item to shopping cart with some properties
        /// </summary>
        /// <param name="item">New shopping item</param>
        /// <param name="tableName">The table of item</param>
        public void AddRowToShoppingCart(ItemToPurchase item, string tableName)
        {
            Logger logger = Logger.getInstance;
            logger.addLog("AddRowToShoppingCart");

            try
            {               
                SqlCommand commandForStack = new SqlCommand();
                commandForStack.Connection = connection;

                commandForStack.CommandText = "Select * From ShoppingCart where ProductId='" + item.Product.ProductId + "'";
                SqlDataReader reader2 = commandForStack.ExecuteReader();

                if (reader2.Read())
                {

                    reader2.Close();
                    SqlCommand commandForQuantity = new SqlCommand("Update ShoppingCart set Quantity=(Quantity + @Quantity) where ProductId= @ProductId", connection);
                    commandForQuantity.Parameters.AddWithValue("@Quantity", item.Quantity);
                    commandForQuantity.Parameters.AddWithValue("@ProductId", item.Product.ProductId);
                    commandForQuantity.ExecuteNonQuery();

                    SqlCommand commandFor = new SqlCommand("Update " + tableName + " set Stock=(Stock - @stock) where ID= @ProductId", connection);
                    commandFor.Parameters.AddWithValue("@Stock", item.Quantity);
                    commandFor.Parameters.AddWithValue("@ProductId", item.Product.ProductId);
                    commandFor.ExecuteNonQuery();
                    MessageBox.Show("Products added to cart!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {                    
                    reader2.Close();
                    ShoppingCart shoppingCart = ShoppingCart.getInstance;
                    shoppingCart.addProduct(item);

                    SqlCommand command = new SqlCommand("Insert into ShoppingCart(ProductId,Name,Quantity,Price,TotalPrice,Type,CustomerID)values (@ProductId,@Name,@Quantity,@Price,@TotalPrice,@Type,@CustomerID)", connection);
                    command.Parameters.AddWithValue("@ProductId", item.Product.ProductId);
                    command.Parameters.AddWithValue("@Name", item.Product.Name);
                    command.Parameters.AddWithValue("@Quantity", item.Quantity);
                    command.Parameters.AddWithValue("@Price", item.Product.Price);
                    command.Parameters.AddWithValue("@TotalPrice", item.TotalPrice);
                    command.Parameters.AddWithValue("@Type", item.Type);
                    command.Parameters.AddWithValue("@CustomerID", item.CustomerID);
                    SqlDataReader reader = command.ExecuteReader();
                    reader.Close();

                    SqlCommand commandForQuantity = new SqlCommand("Update " + tableName + " set Stock=(Stock - @stock) where ID= @ProductId", connection);
                    commandForQuantity.Parameters.AddWithValue("@Stock", item.Quantity);
                    commandForQuantity.Parameters.AddWithValue("@ProductId", item.Product.ProductId);
                    commandForQuantity.ExecuteNonQuery();

                    MessageBox.Show("Products added to cart!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        /// <summary>
        /// Add an order to purchase history
        /// </summary>
        public void AddRowToPurchaseHistory()
        {
            Logger logger = Logger.getInstance;
            logger.addLog("AddRowToPurchaseHistory");

            try
            {
                ShoppingCart shoppingCart = ShoppingCart.getInstance;
                SqlCommand command = new SqlCommand("Insert into Bills(CustomerID,Date,TotalPrice,PaymentType)values (@CustomerID,@Date,@TotalPrice,@PaymentType)", connection);
                command.Parameters.AddWithValue("@CustomerID", shoppingCart.CustomerId);
                command.Parameters.AddWithValue("@Date", DateTime.Now);
                command.Parameters.AddWithValue("@TotalPrice", shoppingCart.PaymentAmount);
                command.Parameters.AddWithValue("@PaymentType", shoppingCart.PaymentType);
                SqlDataReader reader = command.ExecuteReader();
                reader.Close();

                MessageBox.Show("Payment successful!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        /// <summary>
        /// If order is canceled then update the stock with the amount of canceled
        /// </summary>
        /// <param name="amount">Amount of item that is canceled</param>
        /// <param name="productId">ID of item that is canceled</param>
        /// <param name="tableName">The table of item</param>
        public void AddStockBack(int amount, int productId, string tableName)
        {
            Logger logger = Logger.getInstance;
            logger.addLog("AddStockBack");

            tableName = tableName + "s";

            SqlCommand commandForQuantity = new SqlCommand("Update " + tableName + " set Stock=(Stock + @Amount) where ID= @ProductId", connection);
            commandForQuantity.Parameters.AddWithValue("@Amount", amount);
            commandForQuantity.Parameters.AddWithValue("@ProductId", productId);
            commandForQuantity.ExecuteNonQuery();
        }
        /// <summary>
        /// Clear all the orders
        /// </summary>
        public void ClearShoppingCart()
        {
            Logger logger = Logger.getInstance;
            logger.addLog("ClearShoppingCart");

            ShoppingCart shoppingCart = ShoppingCart.getInstance;

            SqlCommand commandForQuantity = new SqlCommand("Delete from ShoppingCart where CustomerID= @CustomerID", connection);
            commandForQuantity.Parameters.AddWithValue("@CustomerID", currentCustomer.Id);
            commandForQuantity.ExecuteNonQuery();
            shoppingCart.cancelOrder();
        }
        /// <summary>
        /// Cancel a specific item with given ID
        /// </summary>
        /// <param name="productid">ID of item that is canceled</param>
        public void RemoveItemFromCart(int productid)
        {
            Logger logger = Logger.getInstance;
            logger.addLog("RemoveItemFromCart");

            ShoppingCart shoppingCart = ShoppingCart.getInstance;
            shoppingCart.removeProduct(productid);

            SqlCommand commandForQuantity = new SqlCommand("Delete  from ShoppingCart where ProductID= @productID", connection);
            commandForQuantity.Parameters.AddWithValue("@productID", productid);
            commandForQuantity.ExecuteNonQuery();
        }
        /// <summary>
        /// Remove a specific item with given ID from the the given table
        /// </summary>
        /// <param name="productid">ID of item that is removed</param>
        /// <param name="tableName">Table of item that is removed</param>
        public void RemoveItemFromDatabase(int productid, string tableName)
        {
            Logger logger = Logger.getInstance;
            logger.addLog("RemoveItemFromDatabase");

            try
            {
                tableName = tableName + "s";
                SqlCommand commandForQuantity = new SqlCommand("Delete  from " + tableName + " where ID= @productID", connection);
                commandForQuantity.Parameters.AddWithValue("@productID", productid);
                commandForQuantity.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
           
        }
        /// <summary>
        /// Update profile of a customer
        /// </summary>
        /// <param name="inputCustomer">Customer that is updated</param>
        public void UpdateProfile(Customer inputCustomer)
        {
            Logger logger = Logger.getInstance;
            logger.addLog("UpdateProfile");

            SqlCommand command = new SqlCommand("Update Customers SET Name=@Name,Surname=@Surname,Address=@Address,Email=@Email,Username=@Username,Password=@Password where ID=@id", connection);
            command.Parameters.AddWithValue("@Name", inputCustomer.Name);
            command.Parameters.AddWithValue("@Surname", inputCustomer.Surname);
            command.Parameters.AddWithValue("@Address", inputCustomer.Adress);
            command.Parameters.AddWithValue("@Email", inputCustomer.Email);
            command.Parameters.AddWithValue("@Username", inputCustomer.Username);
            command.Parameters.AddWithValue("@Password", inputCustomer.Password);
            command.Parameters.AddWithValue("@id", inputCustomer.Id);
            command.ExecuteNonQuery();
        }       
    }
}
