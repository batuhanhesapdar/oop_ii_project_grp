﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oopproje
{
    /// <summary>
    /// Creates an interface for different type of product objects
    /// </summary>
    public abstract class Product
    {
        private string name;
        private int id;
        private int productId;
        private float price;
        private int stock;


        /// <summary>
        /// This function is get and set function for name variable
        /// </summary>
        /// <returns>Returns name variable</returns>
        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }

        /// <summary>
        /// This function is get and set function for id variable
        /// </summary>
        /// <returns>Returns id variable</returns>
        public int Id
        {
            get { return this.id; }
            set { this.id = value; }
        }

        /// <summary>
        /// This function is get and set function for productId variable
        /// </summary>
        /// <returns>Returns productId variable</returns>
        public int ProductId
        {
            get { return this.productId; }
            set { this.productId = value; }
        }

        /// <summary>
        /// This function is get and set function for price variable
        /// </summary>
        /// <returns>Returns price variable</returns>
        public float Price
        {
            get { return this.price; }
            set { this.price = value; }
        }

        /// <summary>
        /// This function is get and set function for stock variable
        /// </summary>
        /// <returns>Returns stock variable</returns>
        public int Stock
        {
            get { return this.stock; }
            set { this.stock = value; }
        }

        /// <summary>
        /// Prints general properties of products
        /// </summary>
        public virtual void printProperties()
        {
            Console.WriteLine($"Product name is {this.name}");
            Console.WriteLine($"Product id is {this.id}");
            Console.WriteLine($"Product price is {this.price}");
        }
    }
    /// <summary>
    /// Implements the product interface for Book
    /// </summary>
    public class Book : Product
    {
        private int isbn;
        private string author;
        private string publisher;
        private int page;

        /// <summary>
        /// This function is get and set function for isbn variable
        /// </summary>
        /// <returns>Returns isbn variable</returns>
        public int ISBN
        {
            get { return this.isbn; }
            set { this.isbn = value; }
        }

        /// <summary>
        /// This function is get and set function for author variable
        /// </summary>
        /// <returns>Returns author variable</returns>
        public string Author
        {
            get { return this.author; }
            set { this.author = value; }
        }

        /// <summary>
        /// This function is get and set function for publisher variable
        /// </summary>
        /// <returns>Returns publisher variable</returns>
        public string Publisher
        {
            get { return this.publisher; }
            set { this.publisher = value; }
        }

        /// <summary>
        /// This function is get and set function for page variable
        /// </summary>
        /// <returns>Returns page variable</returns>
        public int Page
        {
            get { return this.page; }
            set { this.page = value; }
        }

        /// <summary>
        /// Prints properties of Book products
        /// </summary>
        public override void printProperties()
        {
            Logger logger = Logger.getInstance;
            logger.addLog("printProperties");

            base.printProperties();
            Console.WriteLine($"ISBN is {this.isbn}");
            Console.WriteLine($"Author is {this.author}");
            Console.WriteLine($"Publisher is {this.author}");
            Console.WriteLine($"Number of pages are {this.page}");
        }
    }

    enum MagazineType
    {
        Actual,
        News,
        Sport,
        Computer,
        Technology,
        Finance,
        Kids,
        Fashion
    }
    /// <summary>
    /// Implements the product interface for Magazine
    /// </summary>
    class Magazine : Product
    {
        private int issue;
        private MagazineType type;

        /// <summary>
        /// This function is get and set function for issue variable
        /// </summary>
        /// <returns>Returns issue variable</returns>
        public int Issue
        {
            get { return this.issue; }
            set { this.issue = value; }
        }

        /// <summary>
        /// This function is get and set function for type variable
        /// </summary>
        /// <returns>Returns type variable</returns>
        public MagazineType Type
        {
            get { return this.type; }
            set { this.type = value; }
        }

        /// <summary>
        /// Prints properties of Magazine products
        /// </summary>
        public override void printProperties()
        {
            base.printProperties();
            Console.WriteLine($"Issue is {this.issue}");
            Console.WriteLine($"Type is {MagazineTypetoString(this.type)}");
        }

        /// <summary>
        /// Takes a magazine type and convert into string
        /// </summary>
        /// <param name="type">the type of magazine</param>
        /// <returns>String correspond to type</returns>
        public string MagazineTypetoString(MagazineType type)
        {
            switch (type)
            {
                case MagazineType.Actual:
                    return "Actual";
                case MagazineType.Computer:
                    return "Computer";
                case MagazineType.News:
                    return "News";
                case MagazineType.Sport:
                    return "Sport";
                case MagazineType.Technology:
                    return "Technology";
                default:
                    return "Unknown";
            }
        }
    }

    enum MusicType
    {
        Romance,
        HardRock,
        Country,
        Folk,
        Pop
    }
    /// <summary>
    /// Implements the product interface for MusicCD
    /// </summary>
    class MusicCD : Product
    {
        private string singer;
        private MusicType type;
        private string albumName;

        /// <summary>
        /// This function is get and set function for singer variable
        /// </summary>
        /// <returns>Returns singer variable</returns>
        public string Singer
        {
            get { return this.singer; }
            set { this.singer = value; }
        }

        /// <summary>
        /// This function is get and set function for albumName variable
        /// </summary>
        /// <returns>Returns albumName variable</returns>
        public string AlbumName
        {
            get { return this.albumName; }
            set { this.albumName = value; }
        }

        /// <summary>
        /// This function is get and set function for Type variable
        /// </summary>
        /// <returns>Returns Type variable</returns>
        public MusicType Type
        {
            get { return this.type; }
            set { this.type = value; }
        }
        /// <summary>
        /// Prints properties of Music CD products
        /// </summary>
        public override void printProperties()
        {
            base.printProperties();
            Console.WriteLine($"Type is {MusicTypetoString(this.type)}");
            Console.WriteLine($"Singer is {this.singer}");
        }
        /// <summary>
        /// Takes a music type and convert into string
        /// </summary>
        /// <param name="type">the type of music</param>
        /// <returns>String correspond to type</returns>
        public string MusicTypetoString(MusicType type)
        {
            switch (type)
            {
                case MusicType.Country:
                    return "Country";
                case MusicType.Folk:
                    return "Folk";
                case MusicType.HardRock:
                    return "Hard Rock";
                case MusicType.Romance:
                    return "Romance";
                default:
                    return "Unknown";
            }
        }
    }
}
