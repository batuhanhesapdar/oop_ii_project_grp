﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace oopproje
{
    public partial class MainPage : Form
    {
        /// <summary>
        /// Contains a tab view tab collapse all pages
        /// Logs every event in the pages
        /// </summary>
        public MainPage()
        {
            InitializeComponent();
            textBoxPassword.PasswordChar = '●';
            textBoxPasswordC.PasswordChar = '●';
        }
        /// <summary>
        /// Closes the program
        /// </summary>
        private void MainPage_FormClosing(object sender, FormClosingEventArgs e)
        {
            Environment.Exit(0);
        }
        /// <summary>
        /// Click event for add button in book data grid view
        /// </summary>
        private void btnBookAddCart_Click(object sender, EventArgs e)
        {
            Logger logger = Logger.getInstance;
            logger.addLog("btnBookAddCart_Click");

            if (int.Parse(textBoxBookAmount.Text) <= int.Parse(dgvBooks.SelectedRows[0].Cells[7].Value.ToString())) // yeterli stok var mı
            {
                Database database = Database.getDbInstance;
                ItemToPurchase newItem = new ItemToPurchase();

                ProductCreator productCreator = new ProductCreator();
                Book bookProduct = (Book)productCreator.FactoryMethod(ProductType.Book);

                bookProduct.ProductId = int.Parse(dgvBooks.SelectedRows[0].Cells[0].Value.ToString());
                bookProduct.Name = dgvBooks.SelectedRows[0].Cells[1].Value.ToString();
                bookProduct.Price = float.Parse(dgvBooks.SelectedRows[0].Cells[2].Value.ToString());
                bookProduct.ISBN = int.Parse(dgvBooks.SelectedRows[0].Cells[3].Value.ToString());
                bookProduct.Author = dgvBooks.SelectedRows[0].Cells[4].Value.ToString();
                bookProduct.Publisher = dgvBooks.SelectedRows[0].Cells[5].Value.ToString();
                bookProduct.Page = int.Parse(dgvBooks.SelectedRows[0].Cells[6].Value.ToString());
                bookProduct.Stock = int.Parse(dgvBooks.SelectedRows[0].Cells[7].Value.ToString());


                newItem.Product = bookProduct;
                newItem.Quantity = int.Parse(textBoxBookAmount.Text);
                newItem.TotalPrice = int.Parse(textBoxBookAmount.Text) * bookProduct.Price;
                newItem.Type = "Book";
                newItem.CustomerID = database.currentCustomer.Id;

                database.AddRowToShoppingCart(newItem, "Books");

                database.UpdateBookTable();
                DataTable bookTable = database.BookTable;
                dgvBooks.DataSource = bookTable;
            }
            else
            {
                MessageBox.Show("Not enough stock!");
            }
        }
        /// <summary>
        /// Enter event for book page
        /// </summary>
        private void tabPageBook_Enter(object sender, EventArgs e)
        {
            Logger logger = Logger.getInstance;
            logger.addLog("tabPageBook_Enter");

            Database database = Database.getDbInstance;
            database.UpdateBookTable();
            DataTable bookTable = database.BookTable;
            dgvBooks.DataSource = bookTable;
        }
        /// <summary>
        /// Enter event for shoppping cart page
        /// </summary>
        private void tabPageCart_Enter(object sender, EventArgs e)
        {
            Logger logger = Logger.getInstance;
            logger.addLog("tabPageCart_Enter");

            Database database = Database.getDbInstance;
            database.UpdateShoppingCartTable();
            DataTable shoopingCartTable = database.ShoppingCartTable;
            dgvCart.DataSource = shoopingCartTable;
        }
        /// <summary>
        /// Enter event for magazine page
        /// </summary>
        private void tabPageMagazine_Enter(object sender, EventArgs e)
        {
            Logger logger = Logger.getInstance;
            logger.addLog("tabPageMagazine_Enter");

            Database database = Database.getDbInstance;
            database.UpdateMagazineTable();
            DataTable magazineTable = database.MagazinesTable;
            dgvMagazines.DataSource = magazineTable;
        }
        /// <summary>
        /// Enter event for music page
        /// </summary>
        private void tabPageMusic_Enter(object sender, EventArgs e)
        {
            Logger logger = Logger.getInstance;
            logger.addLog("tabPageMusic_Enter");

            Database database = Database.getDbInstance;
            database.UpdateMusicCDTable();
            DataTable magazineTable = database.MusicCDTable;
            dgvMusic.DataSource = magazineTable;
        }
        /// <summary>
        /// Add button for magazine page
        /// </summary>
        private void btnMagAddCart_Click(object sender, EventArgs e)
        {
            Logger logger = Logger.getInstance;
            logger.addLog("btnMagAddCart_Click");

            if (int.Parse(textBoxMagAmount.Text) <= int.Parse(dgvMagazines.SelectedRows[0].Cells[5].Value.ToString())) // yeterli stok var mı
            {
                Database database = Database.getDbInstance;
                ItemToPurchase newItem = new ItemToPurchase();

                ProductCreator productCreator = new ProductCreator();
                Magazine magazineProduct = (Magazine)productCreator.FactoryMethod(ProductType.Magazine);

                magazineProduct.ProductId = int.Parse(dgvMagazines.SelectedRows[0].Cells[0].Value.ToString());
                magazineProduct.Name = dgvMagazines.SelectedRows[0].Cells[1].Value.ToString();
                magazineProduct.Price = float.Parse(dgvMagazines.SelectedRows[0].Cells[2].Value.ToString());

                string tmp = dgvMagazines.SelectedRows[0].Cells[3].Value.ToString();
                Enum.TryParse(tmp, out MagazineType magazineType);
                magazineProduct.Type = magazineType;

                magazineProduct.Issue = int.Parse(dgvMagazines.SelectedRows[0].Cells[4].Value.ToString());
                magazineProduct.Stock = int.Parse(dgvMagazines.SelectedRows[0].Cells[5].Value.ToString());

                newItem.Product = magazineProduct;
                newItem.Quantity = int.Parse(textBoxMagAmount.Text);
                newItem.TotalPrice = int.Parse(textBoxMagAmount.Text) * magazineProduct.Price;
                newItem.Type = "Magazine";
                newItem.CustomerID = database.currentCustomer.Id;

                database.AddRowToShoppingCart(newItem, "Magazines");

                database.UpdateMagazineTable();
                DataTable magazineTable = database.MagazinesTable;
                dgvMagazines.DataSource = magazineTable;
            }
            else
            {
                MessageBox.Show("Not enough stock!");
            }
        }
        /// <summary>
        /// Add button for music page
        /// </summary>
        private void btnMusAddCart_Click(object sender, EventArgs e)
        {
            Logger logger = Logger.getInstance;
            logger.addLog("btnMusAddCart_Click");

            if (int.Parse(textBoxMusAmount.Text) <= int.Parse(dgvMusic.SelectedRows[0].Cells[6].Value.ToString())) // yeterli stok var mı
            {
                Database database = Database.getDbInstance;
                ItemToPurchase newItem = new ItemToPurchase();

                ProductCreator productCreator = new ProductCreator();
                MusicCD musicProduct = (MusicCD)productCreator.FactoryMethod(ProductType.MusicCD);

                musicProduct.ProductId = int.Parse(dgvMusic.SelectedRows[0].Cells[0].Value.ToString());
                musicProduct.Name = dgvMusic.SelectedRows[0].Cells[1].Value.ToString();
                musicProduct.Price = float.Parse(dgvMusic.SelectedRows[0].Cells[2].Value.ToString());
                musicProduct.Singer = dgvMusic.SelectedRows[0].Cells[3].Value.ToString();
                musicProduct.AlbumName = dgvMusic.SelectedRows[0].Cells[4].Value.ToString();

                string tmp = dgvMusic.SelectedRows[0].Cells[5].Value.ToString();
                Enum.TryParse(tmp, out MusicType musicType);
                musicProduct.Type = musicType;

                musicProduct.Stock = int.Parse(dgvMusic.SelectedRows[0].Cells[6].Value.ToString());

                newItem.Product = musicProduct;
                newItem.Quantity = int.Parse(textBoxMusAmount.Text);
                newItem.TotalPrice = int.Parse(textBoxMusAmount.Text) * musicProduct.Price;
                newItem.Type = "Music";
                newItem.CustomerID = database.currentCustomer.Id;

                database.AddRowToShoppingCart(newItem, "Musics");

                database.UpdateMusicCDTable();
                DataTable magazineTable = database.MusicCDTable;
                dgvMusic.DataSource = magazineTable;
            }
            else
            {
                MessageBox.Show("Not enough stock!");
            }
        }
        /// <summary>
        /// Cancel order button in shopping cart
        /// </summary>
        private void btnCancelOrder_Click(object sender, EventArgs e)
        {
            Logger logger = Logger.getInstance;
            logger.addLog("btnCancelOrder_Click");

            Database database = Database.getDbInstance;
            for (int i = 0; i < dgvCart.Rows.Count; i++)
            {
                database.AddStockBack(Int32.Parse(dgvCart.Rows[i].Cells[3].Value.ToString()), Int32.Parse(dgvCart.Rows[i].Cells[1].Value.ToString()), dgvCart.Rows[i].Cells[6].Value.ToString());
            }
            database.ClearShoppingCart();

            database.UpdateShoppingCartTable();
            DataTable shoppingTable = database.ShoppingCartTable;
            dgvCart.DataSource = shoppingTable;
        }
        /// <summary>
        /// Click event for logout in admin panel
        /// </summary>
        private void btnLogout_Click(object sender, EventArgs e)
        {
            Logger logger = Logger.getInstance;
            logger.addLog("btnLogout_Click");

            this.Hide();
            Login login = new Login();
            login.Show();
        }
        /// <summary>
        /// Click event for logout in shopping cart
        /// </summary>
        private void btnSLogOut_Click(object sender, EventArgs e)
        {
            Logger logger = Logger.getInstance;
            logger.addLog("btnSLogOut_Click");

            this.Hide();
            Login login = new Login();
            login.Show();
        }
        /// <summary>
        /// Click event for logout in admin panel
        /// </summary>
        private void btnAdminLogOut_Click(object sender, EventArgs e)
        {
            Logger logger = Logger.getInstance;
            logger.addLog("btnAdminLogOut_Click");

            this.Hide();
            Login login = new Login();
            login.Show();
        }
        /// <summary>
        /// Click event for remove from database
        /// </summary>
        private void btnRemove_Click(object sender, EventArgs e)
        {
            Logger logger = Logger.getInstance;
            logger.addLog("btnRemove_Click");

            Database database = Database.getDbInstance;

            database.AddStockBack(Int32.Parse(dgvCart.SelectedRows[0].Cells[3].Value.ToString()), Int32.Parse(dgvCart.SelectedRows[0].Cells[1].Value.ToString()), dgvCart.SelectedRows[0].Cells[6].Value.ToString());

            database.RemoveItemFromCart(Int32.Parse(dgvCart.SelectedRows[0].Cells[1].Value.ToString()));

            database.UpdateShoppingCartTable();
            DataTable shoppingTable = database.ShoppingCartTable;
            dgvCart.DataSource = shoppingTable;
        }
        /// <summary>
        /// Click event to complete in shopping cart
        /// </summary>
        private void btnCompOrder_Click(object sender, EventArgs e)
        {
            Logger logger = Logger.getInstance;
            logger.addLog("btnCompOrder_Click");

            if (dgvCart.Rows.Count == 0)
            {
                MessageBox.Show("Shopping cart is empty!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                this.Hide();
                PaymentPage paymentPage = new PaymentPage();
                paymentPage.Show();
            }
        }
        /// <summary>
        /// Enter event for account page
        /// </summary>
        private void tabPageAccount_Enter(object sender, EventArgs e)
        {
            Logger logger = Logger.getInstance;
            logger.addLog("tabPageAccount_Enter");

            Database database = Database.getDbInstance;

            textBoxUsername.Text = database.currentCustomer.Username;
            textBoxName.Text = database.currentCustomer.Name;
            textBoxSurname.Text = database.currentCustomer.Surname;
            textBoxEmail.Text = database.currentCustomer.Email;
            textBoxAddress.Text = database.currentCustomer.Adress;
            textBoxPassword.Text = database.currentCustomer.Password;

            database.UpdateBillTable();
            dgvHistory.DataSource = database.BillTable;
        }
        /// <summary>
        /// Click event to update profile
        /// </summary>
        private void btnUpdateP_Click(object sender, EventArgs e)
        {
            Logger logger = Logger.getInstance;
            logger.addLog("btnUpdateP_Click");

            if (!(textBoxPassword.Text == textBoxPasswordC.Text))
            {
                MessageBox.Show("Password are not matching!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                Database database = Database.getDbInstance;
                Customer customer = new Customer();

                customer.Id = database.CurrentCustomer.Id;
                customer.Username = textBoxUsername.Text;
                customer.Name = textBoxName.Text;
                customer.Surname = textBoxSurname.Text;
                customer.Email = textBoxEmail.Text;
                customer.Adress = textBoxAddress.Text;
                customer.Password = textBoxPassword.Text;

                database.UpdateProfile(customer);
            }
        }
        /// <summary>
        /// Track the changes combobox of product types
        /// </summary>
        private void cBoxPType_SelectedIndexChanged(object sender, EventArgs e)
        {
            Logger logger = Logger.getInstance;
            logger.addLog("cBoxPType_SelectedIndexChanged");

            Database database = Database.getDbInstance;
            if (cBoxPType.SelectedIndex.Equals(0))
            {
                groupBoxMusic.Show();
                groupBoxMagazines.Hide();
                groupBoxBook.Hide();

                database.UpdateMusicCDTable();
                dgvAdmin.DataSource = database.MusicCDTable;

            }
            else if (cBoxPType.SelectedIndex.Equals(1))
            {
                groupBoxMusic.Hide();
                groupBoxBook.Hide();
                groupBoxMagazines.Show();

                database.UpdateMagazineTable();
                dgvAdmin.DataSource = database.MagazinesTable;

            }
            else if (cBoxPType.SelectedIndex.Equals(2))
            {
                groupBoxMusic.Hide();
                groupBoxMagazines.Hide();
                groupBoxBook.Show();

                database.UpdateBookTable();
                dgvAdmin.DataSource = database.BookTable;

            }
        }
        /// <summary>
        /// Click event to add products
        /// </summary>
        private void btnAdd_Click(object sender, EventArgs e)
        {
            Logger logger = Logger.getInstance;
            logger.addLog("btnAdd_Click");

            int productID = 444;
            if (cBoxPType.SelectedIndex.Equals(0))
            {
                if (textBoxMusName.Text != String.Empty && textBoxMusPrice.Text != String.Empty && textBoxMusSinger.Text != String.Empty && textBoxMusAlbum.Text != String.Empty
                    && textBoxMusStock.Text != String.Empty)
                {
                    Database database = Database.getDbInstance;
                    ProductCreator productCreator = new ProductCreator();
                    MusicCD musicProduct = (MusicCD)productCreator.FactoryMethod(ProductType.MusicCD);

                    musicProduct.ProductId = 444;
                    musicProduct.Name = textBoxMusName.Text;
                    musicProduct.Price = float.Parse(textBoxMusPrice.Text);
                    musicProduct.Singer = textBoxMusSinger.Text;
                    musicProduct.AlbumName = textBoxMusAlbum.Text;

                    string tmp = cBoxMusic.SelectedItem.ToString();
                    Enum.TryParse(tmp, out MusicType musicType);
                    musicProduct.Type = musicType;                    

                    musicProduct.Stock = int.Parse(textBoxMusStock.Text);
                    database.AddMusicToDatabase(musicProduct);
                    database.UpdateMusicCDTable();

                    DataTable musicTable = database.MusicCDTable;
                    dgvMusic.DataSource = musicTable;

                    productID = Int32.Parse(musicTable.Rows[musicTable.Rows.Count-1].ItemArray[0].ToString());
                }
                else
                {
                    MessageBox.Show("Please fill all the boxes!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else if (cBoxPType.SelectedIndex.Equals(1))
            {
                Database database = Database.getDbInstance;
                ProductCreator productCreator = new ProductCreator();
                Magazine magazineProduct = (Magazine)productCreator.FactoryMethod(ProductType.Magazine);

                magazineProduct.ProductId = 555;
                magazineProduct.Name = textBoxMagName.Text;
                magazineProduct.Price = float.Parse(textBoxMagPrice.Text);
                magazineProduct.Issue = Int32.Parse(textBoxMagIssue.Text);

                string tmp = cBoxMagazine.SelectedItem.ToString();
                Enum.TryParse(tmp, out MagazineType magazineType);
                magazineProduct.Type = magazineType;

                magazineProduct.Stock = int.Parse(textBoxMagStock.Text);
                database.AddMagazineToDatabase(magazineProduct);
                database.UpdateMagazineTable();

                DataTable magazineTable = database.MagazinesTable;
                dgvMagazines.DataSource = magazineTable;

                productID = Int32.Parse(magazineTable.Rows[magazineTable.Rows.Count-1].ItemArray[0].ToString());
            }
            else if (cBoxPType.SelectedIndex.Equals(2))
            {
                Database database = Database.getDbInstance;
                ProductCreator productCreator = new ProductCreator();
                Book bookProduct = (Book)productCreator.FactoryMethod(ProductType.Book);

                bookProduct.ProductId = 666;
                bookProduct.Name = textBoxBookName.Text;
                bookProduct.Price = float.Parse(textBoxBookPrice.Text);
                bookProduct.ISBN = Int32.Parse(textBoxBookISPN.Text);
                bookProduct.Author = textBoxBookAuthor.Text;
                bookProduct.Publisher = textBoxBookPublisher.Text;
                bookProduct.Page = Int32.Parse(textBoxBookPage.Text);

                bookProduct.Stock = int.Parse(textBoxBookStock.Text);
                database.AddBookToDatabase(bookProduct);
                database.UpdateBookTable();

                DataTable bookTable = database.BookTable;
                dgvBooks.DataSource = bookTable;

                productID = Int32.Parse(bookTable.Rows[bookTable.Rows.Count-1].ItemArray[0].ToString());
            }

            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Title = "Select Image.";
            openFileDialog.Filter = ".JPG|*.jpg|PNG|*.png";           

            if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {            
                pictureBoxAdmin.Image = new Bitmap(openFileDialog.FileName);

                string myImageName = productID.ToString();

                string mypath = Environment.CurrentDirectory + "/Picture/" + myImageName + ".png";
                string filePath = System.IO.Path.GetFullPath(openFileDialog.FileName);
                File.Copy(filePath, mypath, true);
            }
            else
            {
                MessageBox.Show("Can't choose image!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        /// <summary>
        /// Click event to delete items
        /// </summary>
        private void btnDelete_Click(object sender, EventArgs e)
        {
            Logger logger = Logger.getInstance;
            logger.addLog("btnDelete_Click");

            Database database = Database.getDbInstance;           

            if (cBoxPType.SelectedIndex.Equals(0))
            {
                database.RemoveItemFromDatabase(int.Parse(dgvAdmin.SelectedRows[0].Cells[0].Value.ToString()), cBoxPType.SelectedItem.ToString());
                database.UpdateMusicCDTable();
                DataTable musicTable = database.MusicCDTable;
                dgvMusic.DataSource = musicTable;
                dgvAdmin.DataSource = musicTable;
            }
            else if (cBoxPType.SelectedIndex.Equals(1))
            {
                database.RemoveItemFromDatabase(int.Parse(dgvAdmin.SelectedRows[0].Cells[0].Value.ToString()), cBoxPType.SelectedItem.ToString());
                database.UpdateMagazineTable();
                DataTable magazineTable = database.MagazinesTable;
                dgvMagazines.DataSource = magazineTable;
                dgvAdmin.DataSource = magazineTable;
            }
            else if (cBoxPType.SelectedIndex.Equals(2))
            {
                database.RemoveItemFromDatabase(int.Parse(dgvAdmin.SelectedRows[0].Cells[0].Value.ToString()), cBoxPType.SelectedItem.ToString());
                database.UpdateBookTable();
                DataTable bookTable = database.BookTable;
                dgvBooks.DataSource = bookTable;
                dgvAdmin.DataSource = bookTable;
            }

        }
        /// <summary>
        /// Click event to update items
        /// </summary>
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            Logger logger = Logger.getInstance;
            logger.addLog("btnUpdate_Click");

            Database database = Database.getDbInstance;
            if (cBoxPType.SelectedIndex.Equals(0))
            {
                ProductCreator productCreator = new ProductCreator();
                MusicCD MusicProduct = (MusicCD)productCreator.FactoryMethod(ProductType.MusicCD);

                MusicProduct.Id = (int)dgvAdmin.SelectedRows[0].Cells[0].Value;
                MusicProduct.Name = textBoxMusName.Text;
                MusicProduct.Price = Int32.Parse(textBoxMusPrice.Text);
                MusicProduct.Singer = textBoxMusSinger.Text;
                MusicProduct.AlbumName = textBoxMusAlbum.Text;
                MusicProduct.Type = (MusicType)cBoxMusic.FindStringExact(cBoxMusic.SelectedItem.ToString());
                MusicProduct.Stock = Int32.Parse(textBoxMusStock.Text);

                database.UpdateMusics(MusicProduct);
             
                database.UpdateMusicCDTable();
                DataTable musicTable = database.MusicCDTable;
                dgvMusic.DataSource = musicTable;
                dgvAdmin.DataSource = musicTable;
            }
            else if (cBoxPType.SelectedIndex.Equals(1))
            {
                ProductCreator productCreator = new ProductCreator();
                Magazine magazineProduct = (Magazine)productCreator.FactoryMethod(ProductType.Magazine);

                magazineProduct.Id = (int)dgvAdmin.SelectedRows[0].Cells[0].Value;
                magazineProduct.Name = textBoxMagName.Text;
                magazineProduct.Price = Int32.Parse(textBoxMagPrice.Text);
                magazineProduct.Type = (MagazineType)cBoxMagazine.FindStringExact((cBoxMagazine.SelectedItem.ToString()));
                magazineProduct.Issue = Int32.Parse(textBoxMagIssue.Text);
                magazineProduct.Stock = Int32.Parse(textBoxMagStock.Text);             

                database.UpdateMagazines(magazineProduct);

                database.UpdateMagazineTable();
                DataTable magTable = database.MagazinesTable;
                dgvMagazines.DataSource = magTable;
                dgvAdmin.DataSource = magTable;
            }
            else if (cBoxPType.SelectedIndex.Equals(2))
            {
                ProductCreator productCreator = new ProductCreator();
                Book bookProduct = (Book)productCreator.FactoryMethod(ProductType.Book);

                bookProduct.Id = (int)dgvAdmin.SelectedRows[0].Cells[0].Value;
                bookProduct.Name = textBoxBookName.Text;
                bookProduct.Price = Int32.Parse(textBoxBookPrice.Text);
                bookProduct.ISBN = Int32.Parse(textBoxBookISPN.Text);
                bookProduct.Author = textBoxBookAuthor.Text;
                bookProduct.Publisher = textBoxBookPublisher.Text;
                bookProduct.Page = Int32.Parse(textBoxBookPage.Text);
                bookProduct.Stock = Int32.Parse(textBoxBookStock.Text);

                database.UpdateBooks(bookProduct);

                database.UpdateBookTable();
                DataTable bookTable = database.BookTable;
                dgvBooks.DataSource = bookTable;
                dgvAdmin.DataSource = bookTable;
            }
        }
        /// <summary>
        /// Track the changes in admin panel
        /// </summary>
        private void dgvAdmin_SelectionChanged(object sender, EventArgs e)
        {
            Logger logger = Logger.getInstance;
            logger.addLog("dgvAdmin_SelectionChanged");

            if (cBoxPType.SelectedIndex.Equals(0))
            {
                if (dgvAdmin.SelectedRows.Count > 0)
                {
                    textBoxMusName.Text = dgvAdmin.SelectedRows[0].Cells[1].Value.ToString();
                    textBoxMusPrice.Text = dgvAdmin.SelectedRows[0].Cells[2].Value.ToString();
                    textBoxMusSinger.Text = dgvAdmin.SelectedRows[0].Cells[3].Value.ToString();
                    textBoxMusAlbum.Text = dgvAdmin.SelectedRows[0].Cells[4].Value.ToString();                    
                    cBoxMusic.SelectedIndex = cBoxMusic.FindStringExact(dgvAdmin.SelectedRows[0].Cells[5].Value.ToString());
                    textBoxMusStock.Text = dgvAdmin.SelectedRows[0].Cells[6].Value.ToString();
                }

            }
            else if (cBoxPType.SelectedIndex.Equals(1))
            {
                if (dgvAdmin.SelectedRows.Count > 0)
                {
                    textBoxMagName.Text = dgvAdmin.SelectedRows[0].Cells[1].Value.ToString();
                    textBoxMagPrice.Text = dgvAdmin.SelectedRows[0].Cells[2].Value.ToString();
                    cBoxMagazine.SelectedIndex = cBoxMagazine.FindStringExact(dgvAdmin.SelectedRows[0].Cells[3].Value.ToString());
                    textBoxMagIssue.Text = dgvAdmin.SelectedRows[0].Cells[4].Value.ToString();
                    textBoxMagStock.Text = dgvAdmin.SelectedRows[0].Cells[5].Value.ToString();                   
                }
            }
            else if (cBoxPType.SelectedIndex.Equals(2))
            {
                if (dgvAdmin.SelectedRows.Count > 0)
                {
                    textBoxBookName.Text = dgvAdmin.SelectedRows[0].Cells[1].Value.ToString();
                    textBoxBookPrice.Text = dgvAdmin.SelectedRows[0].Cells[2].Value.ToString();                    
                    textBoxBookISPN.Text = dgvAdmin.SelectedRows[0].Cells[3].Value.ToString();
                    textBoxBookAuthor.Text = dgvAdmin.SelectedRows[0].Cells[4].Value.ToString();
                    textBoxBookPublisher.Text = dgvAdmin.SelectedRows[0].Cells[5].Value.ToString();
                    textBoxBookPage.Text = dgvAdmin.SelectedRows[0].Cells[6].Value.ToString();
                    textBoxBookStock.Text = dgvAdmin.SelectedRows[0].Cells[7].Value.ToString();
                }
            }
        }
        /// <summary>
        /// Cell Click event in admin panel
        /// </summary>
        private void dgvAdmin_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            Logger logger = Logger.getInstance;
            logger.addLog("dgvAdmin_CellMouseClick");

            string myImageName = dgvAdmin.SelectedRows[0].Cells[0].Value.ToString();
            string mypath = Environment.CurrentDirectory + "/Picture/" + myImageName + ".png";
            Image myimage = Image.FromFile(mypath);
            pictureBoxAdmin.Image = myimage;
        }
        /// <summary>
        /// Cell Click event in books page
        /// </summary>
        private void dgvBooks_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            Logger logger = Logger.getInstance;
            logger.addLog("dgvBooks_CellMouseClick");

            string myImageName = dgvBooks.SelectedRows[0].Cells[0].Value.ToString();
            string mypath = Environment.CurrentDirectory + "/Picture/" + myImageName + ".png";
            Image myimage = Image.FromFile(mypath);
            pictureBoxBooks.Image = myimage;
        }
        /// <summary>
        /// Cell Click event in magazines page
        /// </summary>
        private void dgvMagazines_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            Logger logger = Logger.getInstance;
            logger.addLog("dgvMagazines_CellMouseClick");

            string myImageName = dgvMagazines.SelectedRows[0].Cells[0].Value.ToString();
            string mypath = Environment.CurrentDirectory + "/Picture/" + myImageName + ".png";
            Image myimage = Image.FromFile(mypath);
            pictureBoxMagazines.Image = myimage;
        }
        /// <summary>
        /// Cell Click event in music page
        /// </summary>
        private void dgvMusic_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            Logger logger = Logger.getInstance;
            logger.addLog("dgvMusic_CellMouseClick");

            string myImageName = dgvMusic.SelectedRows[0].Cells[0].Value.ToString();
            string mypath = Environment.CurrentDirectory + "/Picture/" + myImageName + ".png";
            Image myimage = Image.FromFile(mypath);
            pictureBoxMusic.Image = myimage;
        }
        /// <summary>
        /// Cell Click event in shopping cart
        /// </summary>
        private void dgvCart_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            Logger logger = Logger.getInstance;
            logger.addLog("dgvCart_CellMouseClick");

            string myImageName = dgvCart.SelectedRows[0].Cells[1].Value.ToString();
            string mypath = Environment.CurrentDirectory + "/Picture/" + myImageName + ".png";
            Image myimage = Image.FromFile(mypath);
            pictureBoxShopping.Image = myimage;
        }
        /// <summary>
        /// Hide admin tab for normal users
        /// </summary>
        public void hideTab()
        {
            tabControl.TabPages.Remove(tabPageAdmin);
        }
    }
}
