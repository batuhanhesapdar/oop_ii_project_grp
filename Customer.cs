﻿using System;
using System.Collections.Generic;
using System.Text;

namespace oopproje
{
    public class Customer
    {
        /// <summary>
        /// Stores all the data of the user
        /// </summary>
        private int id;
        private string name;
        private string surname;
        private string adress;
        private string email;
        private string username;
        private string password;
        // Default constructor
        public Customer()
        {

        }
        public Customer( string Name, string SurName, string Adress, string Email, string Username, string Password)
        {
        
            name = Name;
            surname = SurName;
            adress = Adress;
            email = Email;
            username = Username;
            password = Password;
        }
        public Customer(int Id,string Name, string SurName, string Adress, string Email, string Username, string Password)
        {
            id = Id;
            name = Name;
            surname = SurName;
            adress = Adress;
            email = Email;
            username = Username;
            password = Password;
        }
        /// <summary>
        /// Creates a string that holds all the properties
        /// </summary>
        /// <returns>Costumer data string</returns>
        public string printCustomerDetails()
        {
            return "Username: " + username + " " + "Password: " + password + " " + "Name: " + name + " " + "Surname: " + surname + " " + "Email: " + email + " " + "Adress: " + adress;
        }
        /// <summary>
        /// Save customer data to database
        /// </summary>
        public void saveCustomer()
        {
            Database database = Database.getDbInstance;
            database.RegisterUser(this);
        }

        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        public string Surname
        {
            get { return surname; }
            set { surname = value; }
        }

        public string Adress
        {
            get { return adress; }
            set { adress = value; }
        }

        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        public string Username
        {
            get { return username; }
            set { username = value; }
        }

        public string Password
        {
            get { return password; }
            set { password = value; }
        }
    }
}
