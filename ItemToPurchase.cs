﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oopproje
{
    class ItemToPurchase
    {
        /// <summary>
        /// Stores all properties of items that is purchased
        /// </summary>
        private Product product;
        private int quantity;
        private float price;
        private float totalPrice;
        private string type;
        private int customerID;

        public Product Product
        {
            get { return product; }
            set { product = value; }
        }

        public int Quantity
        {
            get { return quantity; }
            set { quantity = value; }
        }

        public float Price
        {
            get { return price; }
            set { price = value; }
        }

        public float TotalPrice
        {
            get { return totalPrice; }
            set { totalPrice = value; }
        }

        public string Type
        {
            get { return type; }
            set { type = value; }
        }

        public int CustomerID
        {
            get { return customerID; }
            set { customerID = value; }
        }
    }
}
