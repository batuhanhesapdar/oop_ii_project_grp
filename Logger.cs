﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace oopproje
{
    class Logger
    {
        /// <summary>
        /// Class for taking logs of mouse moments
        /// </summary>
        private static Logger Instance;
        /// <summary>
        /// Singleton design pattern for logger object
        /// </summary>       
        public static Logger getInstance
        {
            get
            {
                if (Instance == null)
                {
                    Instance = new Logger();
                }
                return Instance;
            }
        }
        /// <summary>
        /// Adds logs to txt file
        /// </summary>
        /// <param name="tmp">Logined customer username</param>
        public void addLog(string tmp)
        {
            string mypath = Environment.CurrentDirectory + "/Logs/Logs.txt";
            tmp = tmp + " " + DateTime.Now;
            File.AppendAllText(mypath, tmp + Environment.NewLine);
        }

    }
}
