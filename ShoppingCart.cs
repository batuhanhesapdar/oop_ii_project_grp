﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Windows.Forms;

namespace oopproje
{
    class ShoppingCart
    {
        /// <summary>
        /// Holds the items that is purchased
        /// A singleton class
        /// </summary>
        private static ArrayList itemsToPurchase = new ArrayList();
        private int customerId;
        private float paymentAmount;
        private string paymentType;
        private static ShoppingCart dbInstance;

        public int CustomerId
        {
            get { return this.customerId; }
        }

        public float PaymentAmount
        {
            get { return this.paymentAmount; }
        }

        public string PaymentType
        {
            get { return this.paymentType; }
            set { paymentType = value; }
        }
        /// <summary>
        /// Creates a string that holds all purchased items
        /// </summary>
        /// <returns>Purchased items string</returns>
        public string GetItemList()
        {
            Logger logger = Logger.getInstance;
            logger.addLog("GetItemList");

            string tmpList = String.Empty;
            for(int i = 0; i < itemsToPurchase.Count; i++)
            {
                ItemToPurchase toPurchase = (ItemToPurchase)itemsToPurchase[i];
                tmpList += toPurchase.Product.Name + " Price: " + toPurchase.Product.Price + " Quantity: " + toPurchase.Quantity + " ";
            }
            return tmpList;
        }

        public static ShoppingCart getInstance
        {

            get
            {
                if (dbInstance == null)
                {
                    dbInstance = new ShoppingCart();
                }
                return dbInstance;
            }
        }

        private ShoppingCart()
        {
            Database database = Database.getDbInstance;
            customerId = database.currentCustomer.Id;            
        }
        /// <summary>
        /// Add products to itemsToPurchase array list
        /// </summary>
        /// <param name="product">Purchased product</param>
        public void addProduct(ItemToPurchase product)
        {
            Logger logger = Logger.getInstance;
            logger.addLog("addProduct");

            try
            {               
                itemsToPurchase.Add(product);            
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        /// <summary>
        /// Remove products from itemsToPurchase array list with ID
        /// </summary>
        /// <param name="productID">ID of the deleted product</param>
        public void removeProduct(int productID)
        {
            Logger logger = Logger.getInstance;
            logger.addLog("removeProduct");

            try
            {                               
                for (int i = 0; i < itemsToPurchase.Count; i++)
                {
                    ItemToPurchase Item = new ItemToPurchase();
                    if (Item.Product.Id == productID)
                    {
                        itemsToPurchase.Remove(Item);
                        break;
                    }
                }                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        
        /// <summary>
        /// This function completes the ordering of the products in the shopping cart.
        /// </summary>
        public void placeOrder()
        {
            Logger logger = Logger.getInstance;
            logger.addLog("placeOrder");

            try
            {
                float totalPrice = 0;
                for (int i = 0; i < itemsToPurchase.Count; i++)
                {
                    ItemToPurchase Item = (ItemToPurchase)itemsToPurchase[i];
                    totalPrice += Item.TotalPrice;
                }
                paymentAmount = totalPrice;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// This function deletes the product int the shopping cart.
        /// </summary>
        public void cancelOrder()
        {
            Logger logger = Logger.getInstance;
            logger.addLog("cancelOrder");

            paymentAmount = 0;
            itemsToPurchase.Clear();
        }        
    }
}
