﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Windows.Forms;

namespace oopproje
{
    public partial class PaymentPage : Form
    {
        /// <summary>
        /// Code of the PaymentPage form
        /// </summary>
        public PaymentPage()
        {
            InitializeComponent();
            GboxCreditC.Enabled = false;
        }
        /// <summary>
        /// Assign the logined customer to current costumer property
        /// </summary>
        /// <param name="sender">Logined customer username</param>
        /// <param name="e">Logined customer username</param>
        private void PaymentPagecs_FormClosing(object sender, FormClosingEventArgs e)
        {
            Logger logger = Logger.getInstance;
            logger.addLog("PaymentPagecs_FormClosing");

            Environment.Exit(0);
        }
        /// <summary>
        /// Cancel button for payment page
        /// </summary>
        /// <param name="sender">Logined customer username</param>
        /// <param name="e">Logined customer username</param>
        private void btnCancel_Click(object sender, EventArgs e)
        {
            Logger logger = Logger.getInstance;
            logger.addLog("btnCancel_Click");

            this.Hide();
            MainPage mainPage = new MainPage();
            mainPage.Show();
        }
        /// <summary>
        /// Checks ComboBox'S selected index
        /// </summary>
        /// <param name="sender">Logined customer username</param>
        /// <param name="e">Logined customer username</param>
        private void CboxPayment_SelectedIndexChanged(object sender, EventArgs e)
        {
            Logger logger = Logger.getInstance;
            logger.addLog("CboxPayment_SelectedIndexChanged");

            ShoppingCart shoppingCart = ShoppingCart.getInstance;
            if (CboxPayment.SelectedIndex.Equals(0))
            {
                shoppingCart.PaymentType = "Credit Card";
                GboxCreditC.Enabled = true;
            }
            else
            {
                shoppingCart.PaymentType = "Cash";
                GboxCreditC.Enabled = false;
            }            
        }
        /// <summary>
        /// Checks email
        /// </summary>
        /// <param name="email">Logined customer username</param>     
        bool IsValidEmail(string email)
        {
            Logger logger = Logger.getInstance;
            logger.addLog("IsValidEmail");

            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }
        /// <summary>
        /// button for completing order
        /// </summary>
        /// <param name="sender">Logined customer username</param>
        /// <param name="e">Logined customer username</param>
        private void btnCPayment_Click(object sender, EventArgs e)
        {
            Logger logger = Logger.getInstance;
            logger.addLog("btnCPayment_Click");

            if (!IsValidEmail(TboxEmail.Text))
            {
                MessageBox.Show("Email is not valid!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                Database database = Database.getDbInstance;
                ShoppingCart shoppingCart = ShoppingCart.getInstance;
                shoppingCart.placeOrder();
                database.AddRowToPurchaseHistory();
                
                try
                {
                    //Smpt Client Details
                    //gmail >> smtp server : smtp.gmail.com, port : 587 , ssl required
                    //yahoo >> smtp server : smtp.mail.yahoo.com, port : 587 , ssl required
                    SmtpClient clientDetails = new SmtpClient();
                    clientDetails.Port = 587;
                    clientDetails.Host = "smtp.gmail.com";
                    clientDetails.EnableSsl = true;
                    clientDetails.DeliveryMethod = SmtpDeliveryMethod.Network;
                    clientDetails.UseDefaultCredentials = false;
                    clientDetails.Credentials = new NetworkCredential("oopoop2project@gmail.com", "Batuhan1112");

                    //Message Details
                    MailMessage mailDetails = new MailMessage();
                    mailDetails.From = new MailAddress("oopoop2project@gmail.com");
                    mailDetails.To.Add(TboxEmail.Text);

                    string name = database.CurrentCustomer.Name;                    
                    float totalPrice = shoppingCart.PaymentAmount;
                    
                    string allProducts = shoppingCart.GetItemList();
                    string date = DateTime.Now.ToString();

                    mailDetails.Subject = "Your İnvoice From Book Store";
                    mailDetails.Body = "Thank you for choosing us for your purchase.\n" +
                    " We are happy to meet your purchase order and value our loyal customers. \n\n" +
                    "name : " + name + "\n" + "date : " + date + "\nproducts : \n" + allProducts + "\n" + "price : " + totalPrice + "\n\n";
                   
                    clientDetails.Send(mailDetails);
                    database.ClearShoppingCart();
                    shoppingCart.cancelOrder();
                    MessageBox.Show("Your mail has been sent.", "Successful", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

                this.Hide();
                MainPage mainPage = new MainPage();
                mainPage.Show();
            }


        }
    }
}
