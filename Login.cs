﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace oopproje
{

    public partial class Login : Form
    {
        /// <summary>
        /// Constructor function of Login Class
        /// </summary>
        public Login()
        {
            InitializeComponent();
            textBoxPassword.PasswordChar = '●';
        }
        /// <summary>
        /// Closing Event function of Login Form </summary>
        /// <param name="sender">is a object variable that hold the specified object</param>
        /// <param name="e">is FormClosingEventArgs type variable that holds the eventargs</param>
        /// <returns>A bool value that indicates the succes of login</returns>
        private void Login_FormClosing(object sender, FormClosingEventArgs e)
        {
            Environment.Exit(0);
        }
        /// <summary>
        /// Click Event function of btnLogin Button that is login button, calls database checker funciton to detect is user exist </summary>
        /// <param name="sender">is a object variable that hold the specified object</param>
        /// <param name="e">is EventArgs type variable that holds the eventargs</param>
        /// <returns>A bool value that indicates the succes of login</returns>
        private void btnLogin_Click(object sender, EventArgs e)
        {
            Logger logger = Logger.getInstance;
            logger.addLog("btnLogin_Click");

            Database database = Database.getDbInstance;

            database.ConnectToDatabase();
            if (textBoxUsername.Text == "admin" && textBoxPassword.Text == "123456")
            {
                database.SetCurrentCustomer(textBoxUsername.Text);
                this.Hide();
                MainPage mainPage = new MainPage();
                mainPage.Show();
            }
            else if (database.LoginUser(textBoxUsername.Text, textBoxPassword.Text))
            {
                database.SetCurrentCustomer(textBoxUsername.Text);
                this.Hide();
                MainPage mainPage = new MainPage();
                mainPage.hideTab();
                mainPage.Show();             
            }
            else
            {
                MessageBox.Show("Wrong password or username!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
           
        }
        /// <summary>
        /// Click Event function of btnSignUp_Click Button that is register button, calls register page </summary>
        /// <param name="sender">is a object variable that hold the specified object</param>
        /// <param name="e">is EventArgs type variable that holds the eventargs</param>
        /// <returns>A bool value that indicates the succes of login</returns>
        private void btnSignUp_Click(object sender, EventArgs e)
        {
            Logger logger = Logger.getInstance;
            logger.addLog("btnSignUp_Click");

            this.Hide();
            SignUp sign = new SignUp();
            sign.Show();
        }
    }
}
