﻿
namespace oopproje
{
    partial class MainPage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPageAccount = new System.Windows.Forms.TabPage();
            this.dgvHistory = new System.Windows.Forms.DataGridView();
            this.label8 = new System.Windows.Forms.Label();
            this.textBoxPasswordC = new System.Windows.Forms.TextBox();
            this.textBoxPassword = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxAddress = new System.Windows.Forms.TextBox();
            this.textBoxEmail = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxSurname = new System.Windows.Forms.TextBox();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxUsername = new System.Windows.Forms.TextBox();
            this.btnLogout = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.btnUpdateP = new System.Windows.Forms.Button();
            this.tabPageBook = new System.Windows.Forms.TabPage();
            this.textBoxBookAmount = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.btnBookAddCart = new System.Windows.Forms.Button();
            this.dgvBooks = new System.Windows.Forms.DataGridView();
            this.pictureBoxBooks = new System.Windows.Forms.PictureBox();
            this.tabPageMagazine = new System.Windows.Forms.TabPage();
            this.dgvMagazines = new System.Windows.Forms.DataGridView();
            this.label28 = new System.Windows.Forms.Label();
            this.textBoxMagAmount = new System.Windows.Forms.TextBox();
            this.btnMagAddCart = new System.Windows.Forms.Button();
            this.pictureBoxMagazines = new System.Windows.Forms.PictureBox();
            this.tabPageMusic = new System.Windows.Forms.TabPage();
            this.dgvMusic = new System.Windows.Forms.DataGridView();
            this.label18 = new System.Windows.Forms.Label();
            this.textBoxMusAmount = new System.Windows.Forms.TextBox();
            this.btnMusAddCart = new System.Windows.Forms.Button();
            this.pictureBoxMusic = new System.Windows.Forms.PictureBox();
            this.tabPageCart = new System.Windows.Forms.TabPage();
            this.dgvCart = new System.Windows.Forms.DataGridView();
            this.btnSLogOut = new System.Windows.Forms.Button();
            this.btnRemove = new System.Windows.Forms.Button();
            this.textBoxUpdateAmount = new System.Windows.Forms.TextBox();
            this.btnUpdateAmount = new System.Windows.Forms.Button();
            this.btnCancelOrder = new System.Windows.Forms.Button();
            this.btnCompOrder = new System.Windows.Forms.Button();
            this.pictureBoxShopping = new System.Windows.Forms.PictureBox();
            this.tabPageAdmin = new System.Windows.Forms.TabPage();
            this.dgvAdmin = new System.Windows.Forms.DataGridView();
            this.btnAdminLogOut = new System.Windows.Forms.Button();
            this.groupBoxMusic = new System.Windows.Forms.GroupBox();
            this.cBoxMusic = new System.Windows.Forms.ComboBox();
            this.textBoxMusStock = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.textBoxMusAlbum = new System.Windows.Forms.TextBox();
            this.textBoxMusSinger = new System.Windows.Forms.TextBox();
            this.textBoxMusPrice = new System.Windows.Forms.TextBox();
            this.textBoxMusName = new System.Windows.Forms.TextBox();
            this.btnDelete = new System.Windows.Forms.Button();
            this.pictureBoxAdmin = new System.Windows.Forms.PictureBox();
            this.groupBoxMagazines = new System.Windows.Forms.GroupBox();
            this.cBoxMagazine = new System.Windows.Forms.ComboBox();
            this.label31 = new System.Windows.Forms.Label();
            this.textBoxMagStock = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.textBoxMagIssue = new System.Windows.Forms.TextBox();
            this.textBoxMagPrice = new System.Windows.Forms.TextBox();
            this.textBoxMagName = new System.Windows.Forms.TextBox();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.groupBoxBook = new System.Windows.Forms.GroupBox();
            this.textBoxBookStock = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.textBoxBookPage = new System.Windows.Forms.TextBox();
            this.textBoxBookPublisher = new System.Windows.Forms.TextBox();
            this.textBoxBookAuthor = new System.Windows.Forms.TextBox();
            this.textBoxBookISPN = new System.Windows.Forms.TextBox();
            this.textBoxBookPrice = new System.Windows.Forms.TextBox();
            this.textBoxBookName = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.cBoxPType = new System.Windows.Forms.ComboBox();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.tabControl.SuspendLayout();
            this.tabPageAccount.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvHistory)).BeginInit();
            this.tabPageBook.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBooks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxBooks)).BeginInit();
            this.tabPageMagazine.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMagazines)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxMagazines)).BeginInit();
            this.tabPageMusic.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMusic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxMusic)).BeginInit();
            this.tabPageCart.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxShopping)).BeginInit();
            this.tabPageAdmin.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAdmin)).BeginInit();
            this.groupBoxMusic.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxAdmin)).BeginInit();
            this.groupBoxMagazines.SuspendLayout();
            this.groupBoxBook.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabPageAccount);
            this.tabControl.Controls.Add(this.tabPageBook);
            this.tabControl.Controls.Add(this.tabPageMagazine);
            this.tabControl.Controls.Add(this.tabPageMusic);
            this.tabControl.Controls.Add(this.tabPageCart);
            this.tabControl.Controls.Add(this.tabPageAdmin);
            this.tabControl.Location = new System.Drawing.Point(12, 12);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(776, 426);
            this.tabControl.TabIndex = 0;
            // 
            // tabPageAccount
            // 
            this.tabPageAccount.Controls.Add(this.dgvHistory);
            this.tabPageAccount.Controls.Add(this.label8);
            this.tabPageAccount.Controls.Add(this.textBoxPasswordC);
            this.tabPageAccount.Controls.Add(this.textBoxPassword);
            this.tabPageAccount.Controls.Add(this.label7);
            this.tabPageAccount.Controls.Add(this.label6);
            this.tabPageAccount.Controls.Add(this.textBoxAddress);
            this.tabPageAccount.Controls.Add(this.textBoxEmail);
            this.tabPageAccount.Controls.Add(this.label5);
            this.tabPageAccount.Controls.Add(this.label4);
            this.tabPageAccount.Controls.Add(this.label3);
            this.tabPageAccount.Controls.Add(this.textBoxSurname);
            this.tabPageAccount.Controls.Add(this.textBoxName);
            this.tabPageAccount.Controls.Add(this.label2);
            this.tabPageAccount.Controls.Add(this.textBoxUsername);
            this.tabPageAccount.Controls.Add(this.btnLogout);
            this.tabPageAccount.Controls.Add(this.label1);
            this.tabPageAccount.Controls.Add(this.btnUpdateP);
            this.tabPageAccount.Location = new System.Drawing.Point(4, 24);
            this.tabPageAccount.Name = "tabPageAccount";
            this.tabPageAccount.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageAccount.Size = new System.Drawing.Size(768, 398);
            this.tabPageAccount.TabIndex = 4;
            this.tabPageAccount.Text = "Account";
            this.tabPageAccount.UseVisualStyleBackColor = true;
            this.tabPageAccount.Enter += new System.EventHandler(this.tabPageAccount_Enter);
            // 
            // dgvHistory
            // 
            this.dgvHistory.BackgroundColor = System.Drawing.Color.White;
            this.dgvHistory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvHistory.Location = new System.Drawing.Point(286, 21);
            this.dgvHistory.Name = "dgvHistory";
            this.dgvHistory.RowTemplate.Height = 25;
            this.dgvHistory.Size = new System.Drawing.Size(476, 374);
            this.dgvHistory.TabIndex = 21;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 225);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(107, 15);
            this.label8.TabIndex = 18;
            this.label8.Text = "Confirm Password:";
            // 
            // textBoxPasswordC
            // 
            this.textBoxPasswordC.Location = new System.Drawing.Point(120, 222);
            this.textBoxPasswordC.Name = "textBoxPasswordC";
            this.textBoxPasswordC.Size = new System.Drawing.Size(148, 23);
            this.textBoxPasswordC.TabIndex = 17;
            // 
            // textBoxPassword
            // 
            this.textBoxPassword.Location = new System.Drawing.Point(120, 193);
            this.textBoxPassword.Name = "textBoxPassword";
            this.textBoxPassword.Size = new System.Drawing.Size(148, 23);
            this.textBoxPassword.TabIndex = 16;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(54, 196);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(60, 15);
            this.label7.TabIndex = 15;
            this.label7.Text = "Password:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(62, 140);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 15);
            this.label6.TabIndex = 14;
            this.label6.Text = "Address:";
            // 
            // textBoxAddress
            // 
            this.textBoxAddress.Location = new System.Drawing.Point(120, 137);
            this.textBoxAddress.Multiline = true;
            this.textBoxAddress.Name = "textBoxAddress";
            this.textBoxAddress.Size = new System.Drawing.Size(148, 50);
            this.textBoxAddress.TabIndex = 13;
            // 
            // textBoxEmail
            // 
            this.textBoxEmail.Location = new System.Drawing.Point(120, 108);
            this.textBoxEmail.Name = "textBoxEmail";
            this.textBoxEmail.ReadOnly = true;
            this.textBoxEmail.Size = new System.Drawing.Size(148, 23);
            this.textBoxEmail.TabIndex = 12;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(75, 111);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(39, 15);
            this.label5.TabIndex = 11;
            this.label5.Text = "Email:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(57, 82);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 15);
            this.label4.TabIndex = 10;
            this.label4.Text = "Surname:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(72, 53);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 15);
            this.label3.TabIndex = 9;
            this.label3.Text = "Name:";
            // 
            // textBoxSurname
            // 
            this.textBoxSurname.Location = new System.Drawing.Point(120, 79);
            this.textBoxSurname.Name = "textBoxSurname";
            this.textBoxSurname.Size = new System.Drawing.Size(148, 23);
            this.textBoxSurname.TabIndex = 8;
            // 
            // textBoxName
            // 
            this.textBoxName.Location = new System.Drawing.Point(120, 50);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(148, 23);
            this.textBoxName.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(286, 3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(99, 15);
            this.label2.TabIndex = 6;
            this.label2.Text = "Purchase History:";
            // 
            // textBoxUsername
            // 
            this.textBoxUsername.Location = new System.Drawing.Point(120, 21);
            this.textBoxUsername.Name = "textBoxUsername";
            this.textBoxUsername.ReadOnly = true;
            this.textBoxUsername.Size = new System.Drawing.Size(148, 23);
            this.textBoxUsername.TabIndex = 5;
            // 
            // btnLogout
            // 
            this.btnLogout.Location = new System.Drawing.Point(51, 317);
            this.btnLogout.Name = "btnLogout";
            this.btnLogout.Size = new System.Drawing.Size(200, 60);
            this.btnLogout.TabIndex = 4;
            this.btnLogout.Text = "Log Out";
            this.btnLogout.UseVisualStyleBackColor = true;
            this.btnLogout.Click += new System.EventHandler(this.btnLogout_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(51, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 15);
            this.label1.TabIndex = 1;
            this.label1.Text = "Username:";
            // 
            // btnUpdateP
            // 
            this.btnUpdateP.Location = new System.Drawing.Point(51, 251);
            this.btnUpdateP.Name = "btnUpdateP";
            this.btnUpdateP.Size = new System.Drawing.Size(200, 60);
            this.btnUpdateP.TabIndex = 0;
            this.btnUpdateP.Text = "Update Profile";
            this.btnUpdateP.UseVisualStyleBackColor = true;
            this.btnUpdateP.Click += new System.EventHandler(this.btnUpdateP_Click);
            // 
            // tabPageBook
            // 
            this.tabPageBook.Controls.Add(this.textBoxBookAmount);
            this.tabPageBook.Controls.Add(this.label29);
            this.tabPageBook.Controls.Add(this.btnBookAddCart);
            this.tabPageBook.Controls.Add(this.dgvBooks);
            this.tabPageBook.Controls.Add(this.pictureBoxBooks);
            this.tabPageBook.Location = new System.Drawing.Point(4, 24);
            this.tabPageBook.Name = "tabPageBook";
            this.tabPageBook.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageBook.Size = new System.Drawing.Size(768, 398);
            this.tabPageBook.TabIndex = 0;
            this.tabPageBook.Text = "Books";
            this.tabPageBook.UseVisualStyleBackColor = true;
            this.tabPageBook.Enter += new System.EventHandler(this.tabPageBook_Enter);
            // 
            // textBoxBookAmount
            // 
            this.textBoxBookAmount.Location = new System.Drawing.Point(588, 306);
            this.textBoxBookAmount.Name = "textBoxBookAmount";
            this.textBoxBookAmount.Size = new System.Drawing.Size(100, 23);
            this.textBoxBookAmount.TabIndex = 5;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(528, 309);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(54, 15);
            this.label29.TabIndex = 4;
            this.label29.Text = "Amount:";
            // 
            // btnBookAddCart
            // 
            this.btnBookAddCart.Location = new System.Drawing.Point(588, 335);
            this.btnBookAddCart.Name = "btnBookAddCart";
            this.btnBookAddCart.Size = new System.Drawing.Size(100, 23);
            this.btnBookAddCart.TabIndex = 3;
            this.btnBookAddCart.Text = "Add to Cart";
            this.btnBookAddCart.UseVisualStyleBackColor = true;
            this.btnBookAddCart.Click += new System.EventHandler(this.btnBookAddCart_Click);
            // 
            // dgvBooks
            // 
            this.dgvBooks.AllowUserToAddRows = false;
            this.dgvBooks.AllowUserToDeleteRows = false;
            this.dgvBooks.AllowUserToResizeColumns = false;
            this.dgvBooks.AllowUserToResizeRows = false;
            this.dgvBooks.BackgroundColor = System.Drawing.Color.White;
            this.dgvBooks.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBooks.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvBooks.Location = new System.Drawing.Point(6, 6);
            this.dgvBooks.Name = "dgvBooks";
            this.dgvBooks.RowHeadersVisible = false;
            this.dgvBooks.RowTemplate.Height = 25;
            this.dgvBooks.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvBooks.Size = new System.Drawing.Size(470, 386);
            this.dgvBooks.TabIndex = 2;
            this.dgvBooks.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvBooks_CellMouseClick);
            // 
            // pictureBoxBooks
            // 
            this.pictureBoxBooks.Location = new System.Drawing.Point(482, 6);
            this.pictureBoxBooks.Name = "pictureBoxBooks";
            this.pictureBoxBooks.Size = new System.Drawing.Size(280, 265);
            this.pictureBoxBooks.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxBooks.TabIndex = 1;
            this.pictureBoxBooks.TabStop = false;
            // 
            // tabPageMagazine
            // 
            this.tabPageMagazine.Controls.Add(this.dgvMagazines);
            this.tabPageMagazine.Controls.Add(this.label28);
            this.tabPageMagazine.Controls.Add(this.textBoxMagAmount);
            this.tabPageMagazine.Controls.Add(this.btnMagAddCart);
            this.tabPageMagazine.Controls.Add(this.pictureBoxMagazines);
            this.tabPageMagazine.Location = new System.Drawing.Point(4, 24);
            this.tabPageMagazine.Name = "tabPageMagazine";
            this.tabPageMagazine.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageMagazine.Size = new System.Drawing.Size(768, 398);
            this.tabPageMagazine.TabIndex = 1;
            this.tabPageMagazine.Text = "Magazines";
            this.tabPageMagazine.UseVisualStyleBackColor = true;
            this.tabPageMagazine.Enter += new System.EventHandler(this.tabPageMagazine_Enter);
            // 
            // dgvMagazines
            // 
            this.dgvMagazines.AllowUserToAddRows = false;
            this.dgvMagazines.AllowUserToDeleteRows = false;
            this.dgvMagazines.AllowUserToResizeColumns = false;
            this.dgvMagazines.AllowUserToResizeRows = false;
            this.dgvMagazines.BackgroundColor = System.Drawing.Color.White;
            this.dgvMagazines.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMagazines.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvMagazines.Location = new System.Drawing.Point(6, 6);
            this.dgvMagazines.Name = "dgvMagazines";
            this.dgvMagazines.RowHeadersVisible = false;
            this.dgvMagazines.RowTemplate.Height = 25;
            this.dgvMagazines.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvMagazines.Size = new System.Drawing.Size(470, 386);
            this.dgvMagazines.TabIndex = 6;
            this.dgvMagazines.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvMagazines_CellMouseClick);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(533, 309);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(54, 15);
            this.label28.TabIndex = 5;
            this.label28.Text = "Amount:";
            // 
            // textBoxMagAmount
            // 
            this.textBoxMagAmount.Location = new System.Drawing.Point(593, 306);
            this.textBoxMagAmount.Name = "textBoxMagAmount";
            this.textBoxMagAmount.Size = new System.Drawing.Size(100, 23);
            this.textBoxMagAmount.TabIndex = 4;
            // 
            // btnMagAddCart
            // 
            this.btnMagAddCart.Location = new System.Drawing.Point(593, 335);
            this.btnMagAddCart.Name = "btnMagAddCart";
            this.btnMagAddCart.Size = new System.Drawing.Size(100, 23);
            this.btnMagAddCart.TabIndex = 3;
            this.btnMagAddCart.Text = "Add to Cart";
            this.btnMagAddCart.UseVisualStyleBackColor = true;
            this.btnMagAddCart.Click += new System.EventHandler(this.btnMagAddCart_Click);
            // 
            // pictureBoxMagazines
            // 
            this.pictureBoxMagazines.Location = new System.Drawing.Point(482, 6);
            this.pictureBoxMagazines.Name = "pictureBoxMagazines";
            this.pictureBoxMagazines.Size = new System.Drawing.Size(280, 265);
            this.pictureBoxMagazines.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxMagazines.TabIndex = 1;
            this.pictureBoxMagazines.TabStop = false;
            // 
            // tabPageMusic
            // 
            this.tabPageMusic.Controls.Add(this.dgvMusic);
            this.tabPageMusic.Controls.Add(this.label18);
            this.tabPageMusic.Controls.Add(this.textBoxMusAmount);
            this.tabPageMusic.Controls.Add(this.btnMusAddCart);
            this.tabPageMusic.Controls.Add(this.pictureBoxMusic);
            this.tabPageMusic.Location = new System.Drawing.Point(4, 24);
            this.tabPageMusic.Name = "tabPageMusic";
            this.tabPageMusic.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageMusic.Size = new System.Drawing.Size(768, 398);
            this.tabPageMusic.TabIndex = 2;
            this.tabPageMusic.Text = "Music CDs";
            this.tabPageMusic.UseVisualStyleBackColor = true;
            this.tabPageMusic.Enter += new System.EventHandler(this.tabPageMusic_Enter);
            // 
            // dgvMusic
            // 
            this.dgvMusic.AllowUserToAddRows = false;
            this.dgvMusic.AllowUserToDeleteRows = false;
            this.dgvMusic.AllowUserToResizeColumns = false;
            this.dgvMusic.AllowUserToResizeRows = false;
            this.dgvMusic.BackgroundColor = System.Drawing.Color.White;
            this.dgvMusic.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMusic.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvMusic.Location = new System.Drawing.Point(6, 6);
            this.dgvMusic.Name = "dgvMusic";
            this.dgvMusic.RowHeadersVisible = false;
            this.dgvMusic.RowTemplate.Height = 25;
            this.dgvMusic.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvMusic.Size = new System.Drawing.Size(470, 386);
            this.dgvMusic.TabIndex = 7;
            this.dgvMusic.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvMusic_CellMouseClick);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(535, 306);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(54, 15);
            this.label18.TabIndex = 4;
            this.label18.Text = "Amount:";
            // 
            // textBoxMusAmount
            // 
            this.textBoxMusAmount.Location = new System.Drawing.Point(595, 303);
            this.textBoxMusAmount.Name = "textBoxMusAmount";
            this.textBoxMusAmount.Size = new System.Drawing.Size(100, 23);
            this.textBoxMusAmount.TabIndex = 3;
            // 
            // btnMusAddCart
            // 
            this.btnMusAddCart.Location = new System.Drawing.Point(595, 332);
            this.btnMusAddCart.Name = "btnMusAddCart";
            this.btnMusAddCart.Size = new System.Drawing.Size(100, 23);
            this.btnMusAddCart.TabIndex = 2;
            this.btnMusAddCart.Text = "Add to Cart";
            this.btnMusAddCart.UseVisualStyleBackColor = true;
            this.btnMusAddCart.Click += new System.EventHandler(this.btnMusAddCart_Click);
            // 
            // pictureBoxMusic
            // 
            this.pictureBoxMusic.Location = new System.Drawing.Point(482, 6);
            this.pictureBoxMusic.Name = "pictureBoxMusic";
            this.pictureBoxMusic.Size = new System.Drawing.Size(280, 265);
            this.pictureBoxMusic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxMusic.TabIndex = 0;
            this.pictureBoxMusic.TabStop = false;
            // 
            // tabPageCart
            // 
            this.tabPageCart.Controls.Add(this.dgvCart);
            this.tabPageCart.Controls.Add(this.btnSLogOut);
            this.tabPageCart.Controls.Add(this.btnRemove);
            this.tabPageCart.Controls.Add(this.textBoxUpdateAmount);
            this.tabPageCart.Controls.Add(this.btnUpdateAmount);
            this.tabPageCart.Controls.Add(this.btnCancelOrder);
            this.tabPageCart.Controls.Add(this.btnCompOrder);
            this.tabPageCart.Controls.Add(this.pictureBoxShopping);
            this.tabPageCart.Location = new System.Drawing.Point(4, 24);
            this.tabPageCart.Name = "tabPageCart";
            this.tabPageCart.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageCart.Size = new System.Drawing.Size(768, 398);
            this.tabPageCart.TabIndex = 3;
            this.tabPageCart.Text = "Shopping Cart";
            this.tabPageCart.UseVisualStyleBackColor = true;
            this.tabPageCart.Enter += new System.EventHandler(this.tabPageCart_Enter);
            // 
            // dgvCart
            // 
            this.dgvCart.AllowUserToAddRows = false;
            this.dgvCart.AllowUserToDeleteRows = false;
            this.dgvCart.AllowUserToResizeColumns = false;
            this.dgvCart.AllowUserToResizeRows = false;
            this.dgvCart.BackgroundColor = System.Drawing.Color.White;
            this.dgvCart.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCart.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvCart.Location = new System.Drawing.Point(6, 6);
            this.dgvCart.Name = "dgvCart";
            this.dgvCart.RowHeadersVisible = false;
            this.dgvCart.RowTemplate.Height = 25;
            this.dgvCart.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvCart.Size = new System.Drawing.Size(470, 386);
            this.dgvCart.TabIndex = 3;
            this.dgvCart.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvCart_CellMouseClick);
            // 
            // btnSLogOut
            // 
            this.btnSLogOut.Location = new System.Drawing.Point(494, 337);
            this.btnSLogOut.Name = "btnSLogOut";
            this.btnSLogOut.Size = new System.Drawing.Size(105, 23);
            this.btnSLogOut.TabIndex = 7;
            this.btnSLogOut.Text = "Log Out";
            this.btnSLogOut.UseVisualStyleBackColor = true;
            this.btnSLogOut.Click += new System.EventHandler(this.btnSLogOut_Click);
            // 
            // btnRemove
            // 
            this.btnRemove.Location = new System.Drawing.Point(643, 337);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(105, 23);
            this.btnRemove.TabIndex = 6;
            this.btnRemove.Text = "Remove";
            this.btnRemove.UseVisualStyleBackColor = true;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // textBoxUpdateAmount
            // 
            this.textBoxUpdateAmount.Location = new System.Drawing.Point(643, 279);
            this.textBoxUpdateAmount.Name = "textBoxUpdateAmount";
            this.textBoxUpdateAmount.Size = new System.Drawing.Size(105, 23);
            this.textBoxUpdateAmount.TabIndex = 5;
            // 
            // btnUpdateAmount
            // 
            this.btnUpdateAmount.Location = new System.Drawing.Point(643, 308);
            this.btnUpdateAmount.Name = "btnUpdateAmount";
            this.btnUpdateAmount.Size = new System.Drawing.Size(105, 23);
            this.btnUpdateAmount.TabIndex = 4;
            this.btnUpdateAmount.Text = "Update Amount";
            this.btnUpdateAmount.UseVisualStyleBackColor = true;
            // 
            // btnCancelOrder
            // 
            this.btnCancelOrder.Location = new System.Drawing.Point(494, 308);
            this.btnCancelOrder.Name = "btnCancelOrder";
            this.btnCancelOrder.Size = new System.Drawing.Size(105, 23);
            this.btnCancelOrder.TabIndex = 3;
            this.btnCancelOrder.Text = "Cancel Order";
            this.btnCancelOrder.UseVisualStyleBackColor = true;
            this.btnCancelOrder.Click += new System.EventHandler(this.btnCancelOrder_Click);
            // 
            // btnCompOrder
            // 
            this.btnCompOrder.Location = new System.Drawing.Point(494, 279);
            this.btnCompOrder.Name = "btnCompOrder";
            this.btnCompOrder.Size = new System.Drawing.Size(105, 23);
            this.btnCompOrder.TabIndex = 2;
            this.btnCompOrder.Text = "Complete Order";
            this.btnCompOrder.UseVisualStyleBackColor = true;
            this.btnCompOrder.Click += new System.EventHandler(this.btnCompOrder_Click);
            // 
            // pictureBoxShopping
            // 
            this.pictureBoxShopping.Location = new System.Drawing.Point(482, 6);
            this.pictureBoxShopping.Name = "pictureBoxShopping";
            this.pictureBoxShopping.Size = new System.Drawing.Size(280, 265);
            this.pictureBoxShopping.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxShopping.TabIndex = 1;
            this.pictureBoxShopping.TabStop = false;
            // 
            // tabPageAdmin
            // 
            this.tabPageAdmin.Controls.Add(this.dgvAdmin);
            this.tabPageAdmin.Controls.Add(this.btnAdminLogOut);
            this.tabPageAdmin.Controls.Add(this.groupBoxMusic);
            this.tabPageAdmin.Controls.Add(this.btnDelete);
            this.tabPageAdmin.Controls.Add(this.pictureBoxAdmin);
            this.tabPageAdmin.Controls.Add(this.groupBoxMagazines);
            this.tabPageAdmin.Controls.Add(this.btnUpdate);
            this.tabPageAdmin.Controls.Add(this.btnAdd);
            this.tabPageAdmin.Controls.Add(this.groupBoxBook);
            this.tabPageAdmin.Controls.Add(this.label11);
            this.tabPageAdmin.Controls.Add(this.cBoxPType);
            this.tabPageAdmin.Location = new System.Drawing.Point(4, 24);
            this.tabPageAdmin.Name = "tabPageAdmin";
            this.tabPageAdmin.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageAdmin.Size = new System.Drawing.Size(768, 398);
            this.tabPageAdmin.TabIndex = 5;
            this.tabPageAdmin.Text = "Admin Panel";
            this.tabPageAdmin.UseVisualStyleBackColor = true;
            // 
            // dgvAdmin
            // 
            this.dgvAdmin.AllowUserToAddRows = false;
            this.dgvAdmin.AllowUserToDeleteRows = false;
            this.dgvAdmin.AllowUserToResizeColumns = false;
            this.dgvAdmin.AllowUserToResizeRows = false;
            this.dgvAdmin.BackgroundColor = System.Drawing.Color.White;
            this.dgvAdmin.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAdmin.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvAdmin.Location = new System.Drawing.Point(18, 6);
            this.dgvAdmin.Name = "dgvAdmin";
            this.dgvAdmin.RowHeadersVisible = false;
            this.dgvAdmin.RowTemplate.Height = 25;
            this.dgvAdmin.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAdmin.Size = new System.Drawing.Size(470, 386);
            this.dgvAdmin.TabIndex = 12;
            this.dgvAdmin.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvAdmin_CellMouseClick);
            this.dgvAdmin.SelectionChanged += new System.EventHandler(this.dgvAdmin_SelectionChanged);
            // 
            // btnAdminLogOut
            // 
            this.btnAdminLogOut.Location = new System.Drawing.Point(707, 274);
            this.btnAdminLogOut.Name = "btnAdminLogOut";
            this.btnAdminLogOut.Size = new System.Drawing.Size(55, 23);
            this.btnAdminLogOut.TabIndex = 11;
            this.btnAdminLogOut.Text = "LogOut";
            this.btnAdminLogOut.UseVisualStyleBackColor = true;
            this.btnAdminLogOut.Click += new System.EventHandler(this.btnAdminLogOut_Click);
            // 
            // groupBoxMusic
            // 
            this.groupBoxMusic.Controls.Add(this.cBoxMusic);
            this.groupBoxMusic.Controls.Add(this.textBoxMusStock);
            this.groupBoxMusic.Controls.Add(this.label30);
            this.groupBoxMusic.Controls.Add(this.label19);
            this.groupBoxMusic.Controls.Add(this.label24);
            this.groupBoxMusic.Controls.Add(this.label25);
            this.groupBoxMusic.Controls.Add(this.label26);
            this.groupBoxMusic.Controls.Add(this.label27);
            this.groupBoxMusic.Controls.Add(this.textBoxMusAlbum);
            this.groupBoxMusic.Controls.Add(this.textBoxMusSinger);
            this.groupBoxMusic.Controls.Add(this.textBoxMusPrice);
            this.groupBoxMusic.Controls.Add(this.textBoxMusName);
            this.groupBoxMusic.Location = new System.Drawing.Point(557, 59);
            this.groupBoxMusic.Name = "groupBoxMusic";
            this.groupBoxMusic.Size = new System.Drawing.Size(200, 202);
            this.groupBoxMusic.TabIndex = 8;
            this.groupBoxMusic.TabStop = false;
            this.groupBoxMusic.Text = "Music CD\'s Properties";
            this.groupBoxMusic.Visible = false;
            // 
            // cBoxMusic
            // 
            this.cBoxMusic.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBoxMusic.FormattingEnabled = true;
            this.cBoxMusic.Items.AddRange(new object[] {
            "Romance",
            "HardRock",
            "Country",
            "Folk",
            "Pop"});
            this.cBoxMusic.Location = new System.Drawing.Point(94, 138);
            this.cBoxMusic.Name = "cBoxMusic";
            this.cBoxMusic.Size = new System.Drawing.Size(100, 23);
            this.cBoxMusic.TabIndex = 13;
            // 
            // textBoxMusStock
            // 
            this.textBoxMusStock.Location = new System.Drawing.Point(94, 167);
            this.textBoxMusStock.Name = "textBoxMusStock";
            this.textBoxMusStock.Size = new System.Drawing.Size(100, 23);
            this.textBoxMusStock.TabIndex = 12;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(49, 170);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(39, 15);
            this.label30.TabIndex = 11;
            this.label30.Text = "Stock:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(19, 141);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(69, 15);
            this.label19.TabIndex = 10;
            this.label19.Text = "Music Type:";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(7, 112);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(81, 15);
            this.label24.TabIndex = 9;
            this.label24.Text = "Album Name:";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(10, 83);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(78, 15);
            this.label25.TabIndex = 8;
            this.label25.Text = "Singer Name:";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(52, 54);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(36, 15);
            this.label26.TabIndex = 7;
            this.label26.Text = "Price:";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(46, 25);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(42, 15);
            this.label27.TabIndex = 6;
            this.label27.Text = "Name:";
            // 
            // textBoxMusAlbum
            // 
            this.textBoxMusAlbum.Location = new System.Drawing.Point(94, 109);
            this.textBoxMusAlbum.Name = "textBoxMusAlbum";
            this.textBoxMusAlbum.Size = new System.Drawing.Size(100, 23);
            this.textBoxMusAlbum.TabIndex = 3;
            // 
            // textBoxMusSinger
            // 
            this.textBoxMusSinger.Location = new System.Drawing.Point(94, 80);
            this.textBoxMusSinger.Name = "textBoxMusSinger";
            this.textBoxMusSinger.Size = new System.Drawing.Size(100, 23);
            this.textBoxMusSinger.TabIndex = 2;
            // 
            // textBoxMusPrice
            // 
            this.textBoxMusPrice.Location = new System.Drawing.Point(94, 51);
            this.textBoxMusPrice.Name = "textBoxMusPrice";
            this.textBoxMusPrice.Size = new System.Drawing.Size(100, 23);
            this.textBoxMusPrice.TabIndex = 1;
            // 
            // textBoxMusName
            // 
            this.textBoxMusName.Location = new System.Drawing.Point(94, 22);
            this.textBoxMusName.Name = "textBoxMusName";
            this.textBoxMusName.Size = new System.Drawing.Size(100, 23);
            this.textBoxMusName.TabIndex = 0;
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(649, 274);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(52, 23);
            this.btnDelete.TabIndex = 10;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // pictureBoxAdmin
            // 
            this.pictureBoxAdmin.Location = new System.Drawing.Point(534, 303);
            this.pictureBoxAdmin.Name = "pictureBoxAdmin";
            this.pictureBoxAdmin.Size = new System.Drawing.Size(223, 89);
            this.pictureBoxAdmin.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxAdmin.TabIndex = 9;
            this.pictureBoxAdmin.TabStop = false;
            // 
            // groupBoxMagazines
            // 
            this.groupBoxMagazines.Controls.Add(this.cBoxMagazine);
            this.groupBoxMagazines.Controls.Add(this.label31);
            this.groupBoxMagazines.Controls.Add(this.textBoxMagStock);
            this.groupBoxMagazines.Controls.Add(this.label20);
            this.groupBoxMagazines.Controls.Add(this.label21);
            this.groupBoxMagazines.Controls.Add(this.label22);
            this.groupBoxMagazines.Controls.Add(this.label23);
            this.groupBoxMagazines.Controls.Add(this.textBoxMagIssue);
            this.groupBoxMagazines.Controls.Add(this.textBoxMagPrice);
            this.groupBoxMagazines.Controls.Add(this.textBoxMagName);
            this.groupBoxMagazines.Location = new System.Drawing.Point(545, 67);
            this.groupBoxMagazines.Name = "groupBoxMagazines";
            this.groupBoxMagazines.Size = new System.Drawing.Size(180, 188);
            this.groupBoxMagazines.TabIndex = 7;
            this.groupBoxMagazines.TabStop = false;
            this.groupBoxMagazines.Text = "Magazine\'s Properties";
            this.groupBoxMagazines.Visible = false;
            // 
            // cBoxMagazine
            // 
            this.cBoxMagazine.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBoxMagazine.FormattingEnabled = true;
            this.cBoxMagazine.Items.AddRange(new object[] {
            "Actual",
            "News",
            "Sport",
            "Computer",
            "Technology",
            "Finance",
            "Kids"});
            this.cBoxMagazine.Location = new System.Drawing.Point(61, 95);
            this.cBoxMagazine.Name = "cBoxMagazine";
            this.cBoxMagazine.Size = new System.Drawing.Size(100, 23);
            this.cBoxMagazine.TabIndex = 14;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(16, 156);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(39, 15);
            this.label31.TabIndex = 11;
            this.label31.Text = "Stock:";
            // 
            // textBoxMagStock
            // 
            this.textBoxMagStock.Location = new System.Drawing.Point(61, 153);
            this.textBoxMagStock.Name = "textBoxMagStock";
            this.textBoxMagStock.Size = new System.Drawing.Size(100, 23);
            this.textBoxMagStock.TabIndex = 10;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(19, 127);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(36, 15);
            this.label20.TabIndex = 9;
            this.label20.Text = "Issue:";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(20, 98);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(34, 15);
            this.label21.TabIndex = 8;
            this.label21.Text = "Type:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(19, 69);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(36, 15);
            this.label22.TabIndex = 7;
            this.label22.Text = "Price:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(13, 40);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(42, 15);
            this.label23.TabIndex = 6;
            this.label23.Text = "Name:";
            // 
            // textBoxMagIssue
            // 
            this.textBoxMagIssue.Location = new System.Drawing.Point(61, 124);
            this.textBoxMagIssue.Name = "textBoxMagIssue";
            this.textBoxMagIssue.Size = new System.Drawing.Size(100, 23);
            this.textBoxMagIssue.TabIndex = 3;
            // 
            // textBoxMagPrice
            // 
            this.textBoxMagPrice.Location = new System.Drawing.Point(61, 66);
            this.textBoxMagPrice.Name = "textBoxMagPrice";
            this.textBoxMagPrice.Size = new System.Drawing.Size(100, 23);
            this.textBoxMagPrice.TabIndex = 1;
            // 
            // textBoxMagName
            // 
            this.textBoxMagName.Location = new System.Drawing.Point(61, 37);
            this.textBoxMagName.Name = "textBoxMagName";
            this.textBoxMagName.Size = new System.Drawing.Size(100, 23);
            this.textBoxMagName.TabIndex = 0;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(590, 274);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(53, 23);
            this.btnUpdate.TabIndex = 7;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(534, 274);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(50, 23);
            this.btnAdd.TabIndex = 5;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // groupBoxBook
            // 
            this.groupBoxBook.Controls.Add(this.textBoxBookStock);
            this.groupBoxBook.Controls.Add(this.label32);
            this.groupBoxBook.Controls.Add(this.label17);
            this.groupBoxBook.Controls.Add(this.label16);
            this.groupBoxBook.Controls.Add(this.label15);
            this.groupBoxBook.Controls.Add(this.label14);
            this.groupBoxBook.Controls.Add(this.label13);
            this.groupBoxBook.Controls.Add(this.label12);
            this.groupBoxBook.Controls.Add(this.textBoxBookPage);
            this.groupBoxBook.Controls.Add(this.textBoxBookPublisher);
            this.groupBoxBook.Controls.Add(this.textBoxBookAuthor);
            this.groupBoxBook.Controls.Add(this.textBoxBookISPN);
            this.groupBoxBook.Controls.Add(this.textBoxBookPrice);
            this.groupBoxBook.Controls.Add(this.textBoxBookName);
            this.groupBoxBook.Location = new System.Drawing.Point(563, 38);
            this.groupBoxBook.Name = "groupBoxBook";
            this.groupBoxBook.Size = new System.Drawing.Size(188, 229);
            this.groupBoxBook.TabIndex = 6;
            this.groupBoxBook.TabStop = false;
            this.groupBoxBook.Text = "Book\'s Properties";
            this.groupBoxBook.Visible = false;
            // 
            // textBoxBookStock
            // 
            this.textBoxBookStock.Location = new System.Drawing.Point(72, 196);
            this.textBoxBookStock.Name = "textBoxBookStock";
            this.textBoxBookStock.Size = new System.Drawing.Size(100, 23);
            this.textBoxBookStock.TabIndex = 13;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(27, 199);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(39, 15);
            this.label32.TabIndex = 12;
            this.label32.Text = "Stock:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(30, 170);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(36, 15);
            this.label17.TabIndex = 11;
            this.label17.Text = "Page:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(7, 141);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(59, 15);
            this.label16.TabIndex = 10;
            this.label16.Text = "Publisher:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(19, 112);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(47, 15);
            this.label15.TabIndex = 9;
            this.label15.Text = "Author:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(31, 83);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(35, 15);
            this.label14.TabIndex = 8;
            this.label14.Text = "ISBN:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(30, 54);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(36, 15);
            this.label13.TabIndex = 7;
            this.label13.Text = "Price:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(24, 25);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(42, 15);
            this.label12.TabIndex = 6;
            this.label12.Text = "Name:";
            // 
            // textBoxBookPage
            // 
            this.textBoxBookPage.Location = new System.Drawing.Point(72, 167);
            this.textBoxBookPage.Name = "textBoxBookPage";
            this.textBoxBookPage.Size = new System.Drawing.Size(100, 23);
            this.textBoxBookPage.TabIndex = 5;
            // 
            // textBoxBookPublisher
            // 
            this.textBoxBookPublisher.Location = new System.Drawing.Point(72, 138);
            this.textBoxBookPublisher.Name = "textBoxBookPublisher";
            this.textBoxBookPublisher.Size = new System.Drawing.Size(100, 23);
            this.textBoxBookPublisher.TabIndex = 4;
            // 
            // textBoxBookAuthor
            // 
            this.textBoxBookAuthor.Location = new System.Drawing.Point(72, 109);
            this.textBoxBookAuthor.Name = "textBoxBookAuthor";
            this.textBoxBookAuthor.Size = new System.Drawing.Size(100, 23);
            this.textBoxBookAuthor.TabIndex = 3;
            // 
            // textBoxBookISPN
            // 
            this.textBoxBookISPN.Location = new System.Drawing.Point(72, 80);
            this.textBoxBookISPN.Name = "textBoxBookISPN";
            this.textBoxBookISPN.Size = new System.Drawing.Size(100, 23);
            this.textBoxBookISPN.TabIndex = 2;
            // 
            // textBoxBookPrice
            // 
            this.textBoxBookPrice.Location = new System.Drawing.Point(72, 51);
            this.textBoxBookPrice.Name = "textBoxBookPrice";
            this.textBoxBookPrice.Size = new System.Drawing.Size(100, 23);
            this.textBoxBookPrice.TabIndex = 1;
            // 
            // textBoxBookName
            // 
            this.textBoxBookName.Location = new System.Drawing.Point(72, 22);
            this.textBoxBookName.Name = "textBoxBookName";
            this.textBoxBookName.Size = new System.Drawing.Size(100, 23);
            this.textBoxBookName.TabIndex = 0;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(545, 9);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(79, 15);
            this.label11.TabIndex = 3;
            this.label11.Text = "Product Type:";
            // 
            // cBoxPType
            // 
            this.cBoxPType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBoxPType.FormattingEnabled = true;
            this.cBoxPType.Items.AddRange(new object[] {
            "Music",
            "Magazine",
            "Book"});
            this.cBoxPType.Location = new System.Drawing.Point(630, 6);
            this.cBoxPType.Name = "cBoxPType";
            this.cBoxPType.Size = new System.Drawing.Size(121, 23);
            this.cBoxPType.TabIndex = 2;
            this.cBoxPType.SelectedIndexChanged += new System.EventHandler(this.cBoxPType_SelectedIndexChanged);
            // 
            // MainPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.tabControl);
            this.Name = "MainPage";
            this.Text = "Main Page";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainPage_FormClosing);
            this.tabControl.ResumeLayout(false);
            this.tabPageAccount.ResumeLayout(false);
            this.tabPageAccount.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvHistory)).EndInit();
            this.tabPageBook.ResumeLayout(false);
            this.tabPageBook.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBooks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxBooks)).EndInit();
            this.tabPageMagazine.ResumeLayout(false);
            this.tabPageMagazine.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMagazines)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxMagazines)).EndInit();
            this.tabPageMusic.ResumeLayout(false);
            this.tabPageMusic.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMusic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxMusic)).EndInit();
            this.tabPageCart.ResumeLayout(false);
            this.tabPageCart.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxShopping)).EndInit();
            this.tabPageAdmin.ResumeLayout(false);
            this.tabPageAdmin.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAdmin)).EndInit();
            this.groupBoxMusic.ResumeLayout(false);
            this.groupBoxMusic.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxAdmin)).EndInit();
            this.groupBoxMagazines.ResumeLayout(false);
            this.groupBoxMagazines.PerformLayout();
            this.groupBoxBook.ResumeLayout(false);
            this.groupBoxBook.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPageBook;
        private System.Windows.Forms.TabPage tabPageMagazine;
        private System.Windows.Forms.TabPage tabPageMusic;
        private System.Windows.Forms.TabPage tabPageCart;
        private System.Windows.Forms.PictureBox pictureBoxBooks;
        private System.Windows.Forms.TabPage tabPageAccount;
        private System.Windows.Forms.PictureBox pictureBoxMagazines;
        private System.Windows.Forms.PictureBox pictureBoxMusic;
        private System.Windows.Forms.PictureBox pictureBoxShopping;
        private System.Windows.Forms.Button btnLogout;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnUpdateP;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxAddress;
        private System.Windows.Forms.TextBox textBoxEmail;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxSurname;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxUsername;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBoxPasswordC;
        private System.Windows.Forms.TextBox textBoxPassword;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnCancelOrder;
        private System.Windows.Forms.Button btnCompOrder;
        private System.Windows.Forms.Button btnRemove;
        private System.Windows.Forms.TextBox textBoxUpdateAmount;
        private System.Windows.Forms.Button btnUpdateAmount;
        private System.Windows.Forms.Button btnSLogOut;
        private System.Windows.Forms.TabPage tabPageAdmin;
        private System.Windows.Forms.DataGridView dgvBooks;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cBoxPType;
        private System.Windows.Forms.DataGridView dgvHistory;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.GroupBox groupBoxBook;
        private System.Windows.Forms.TextBox textBoxBookPage;
        private System.Windows.Forms.TextBox textBoxBookPublisher;
        private System.Windows.Forms.TextBox textBoxBookAuthor;
        private System.Windows.Forms.TextBox textBoxBookISPN;
        private System.Windows.Forms.TextBox textBoxBookPrice;
        private System.Windows.Forms.TextBox textBoxBookName;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.GroupBox groupBoxMagazines;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox textBoxMagIssue;
        private System.Windows.Forms.TextBox textBoxMagPrice;
        private System.Windows.Forms.TextBox textBoxMagName;
        private System.Windows.Forms.GroupBox groupBoxMusic;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox textBoxMusAlbum;
        private System.Windows.Forms.TextBox textBoxMusSinger;
        private System.Windows.Forms.TextBox textBoxMusPrice;
        private System.Windows.Forms.TextBox textBoxMusName;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.PictureBox pictureBoxAdmin;
        private System.Windows.Forms.Button btnAdminLogOut;
        private System.Windows.Forms.Button btnBookAddCart;
        private System.Windows.Forms.TextBox textBoxMagAmount;
        private System.Windows.Forms.Button btnMagAddCart;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox textBoxMusAmount;
        private System.Windows.Forms.Button btnMusAddCart;
        private System.Windows.Forms.TextBox textBoxBookAmount;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox textBoxMusStock;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox textBoxMagStock;
        private System.Windows.Forms.TextBox textBoxBookStock;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.DataGridView dgvCart;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.DataGridView dgvMagazines;
        private System.Windows.Forms.DataGridView dgvMusic;
        private System.Windows.Forms.ComboBox cBoxMusic;
        private System.Windows.Forms.ComboBox cBoxMagazine;
        private System.Windows.Forms.DataGridView dgvAdmin;
    }
}