﻿using System;
using System.Collections.Generic;
using System.Text;

namespace oopproje
{
    /// <summary>
    /// This class has a factory method to create new product objects
    /// </summary>
    class ProductCreator
    {
        public Product FactoryMethod(ProductType productType)
        {
            Product product = null;
            switch (productType)
            {
                case ProductType.Book:
                    product = new Book();
                    break;
                case ProductType.Magazine:
                    product = new Magazine();
                    break;
                case ProductType.MusicCD:
                    product = new MusicCD();
                    break;
                default:
                    break;
            }
            return product;
        }
    }
    enum ProductType
    {
        Magazine,
        Book,
        MusicCD
    }
}
