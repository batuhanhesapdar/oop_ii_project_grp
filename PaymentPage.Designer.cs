﻿
namespace oopproje
{
    partial class PaymentPage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CboxPayment = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.GboxAddress = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.TboxEmail = new System.Windows.Forms.TextBox();
            this.CboxCountry = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.TboxPhone = new System.Windows.Forms.TextBox();
            this.TboxZIP = new System.Windows.Forms.TextBox();
            this.TboxRegion = new System.Windows.Forms.TextBox();
            this.TboxCity = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.TboxAddress2 = new System.Windows.Forms.TextBox();
            this.TboxAddress1 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.TboxFullName = new System.Windows.Forms.TextBox();
            this.GboxCreditC = new System.Windows.Forms.GroupBox();
            this.TboxCVV = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.CboxYear = new System.Windows.Forms.ComboBox();
            this.CboxMonth = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.TboxCardNumber = new System.Windows.Forms.TextBox();
            this.TboxCardName = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.btnCPayment = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.GboxAddress.SuspendLayout();
            this.GboxCreditC.SuspendLayout();
            this.SuspendLayout();
            // 
            // CboxPayment
            // 
            this.CboxPayment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CboxPayment.FormattingEnabled = true;
            this.CboxPayment.Items.AddRange(new object[] {
            "Credit Card",
            "Cash"});
            this.CboxPayment.Location = new System.Drawing.Point(322, 341);
            this.CboxPayment.Name = "CboxPayment";
            this.CboxPayment.Size = new System.Drawing.Size(121, 23);
            this.CboxPayment.TabIndex = 0;
            this.CboxPayment.SelectedIndexChanged += new System.EventHandler(this.CboxPayment_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(214, 344);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(102, 15);
            this.label1.TabIndex = 1;
            this.label1.Text = "Payment Method:";
            // 
            // GboxAddress
            // 
            this.GboxAddress.Controls.Add(this.label11);
            this.GboxAddress.Controls.Add(this.label10);
            this.GboxAddress.Controls.Add(this.TboxEmail);
            this.GboxAddress.Controls.Add(this.CboxCountry);
            this.GboxAddress.Controls.Add(this.label9);
            this.GboxAddress.Controls.Add(this.label8);
            this.GboxAddress.Controls.Add(this.label7);
            this.GboxAddress.Controls.Add(this.label6);
            this.GboxAddress.Controls.Add(this.TboxPhone);
            this.GboxAddress.Controls.Add(this.TboxZIP);
            this.GboxAddress.Controls.Add(this.TboxRegion);
            this.GboxAddress.Controls.Add(this.TboxCity);
            this.GboxAddress.Controls.Add(this.label5);
            this.GboxAddress.Controls.Add(this.label4);
            this.GboxAddress.Controls.Add(this.TboxAddress2);
            this.GboxAddress.Controls.Add(this.TboxAddress1);
            this.GboxAddress.Controls.Add(this.label3);
            this.GboxAddress.Controls.Add(this.label2);
            this.GboxAddress.Controls.Add(this.TboxFullName);
            this.GboxAddress.Location = new System.Drawing.Point(39, 12);
            this.GboxAddress.Name = "GboxAddress";
            this.GboxAddress.Size = new System.Drawing.Size(570, 323);
            this.GboxAddress.TabIndex = 2;
            this.GboxAddress.TabStop = false;
            this.GboxAddress.Text = "Address";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(203, 292);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(218, 15);
            this.label11.TabIndex = 4;
            this.label11.Text = "Note: Invoice will be send to your email.";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(158, 269);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(39, 15);
            this.label10.TabIndex = 18;
            this.label10.Text = "Email:";
            // 
            // TboxEmail
            // 
            this.TboxEmail.Location = new System.Drawing.Point(203, 266);
            this.TboxEmail.Name = "TboxEmail";
            this.TboxEmail.Size = new System.Drawing.Size(285, 23);
            this.TboxEmail.TabIndex = 17;
            // 
            // CboxCountry
            // 
            this.CboxCountry.DropDownHeight = 150;
            this.CboxCountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CboxCountry.FormattingEnabled = true;
            this.CboxCountry.IntegralHeight = false;
            this.CboxCountry.Items.AddRange(new object[] {
            "Afghanistan",
            "Albania",
            "Algeria",
            "Andorra",
            "Angola",
            "Antigua and Barbuda",
            "Argentina",
            "Armenia",
            "Australia",
            "Austria",
            "Azerbaijan",
            "The Bahamas",
            "Bahrain",
            "Bangladesh",
            "Barbados",
            "Belarus",
            "Belgium",
            "Belize",
            "Benin",
            "Bhutan",
            "Bolivia",
            "Bosnia and Herzegovina",
            "Botswana",
            "Brazil",
            "Brunei",
            "Bulgaria",
            "Burkina Faso",
            "Burundi",
            "Cabo Verde",
            "Cambodia",
            "Cameroon",
            "Canada",
            "Central African Republic",
            "Chad",
            "Chile",
            "China",
            "Colombia",
            "Comoros",
            "Congo, Democratic Republic of the",
            "Congo, Republic of the",
            "Costa Rica",
            "Côte d’Ivoire",
            "Croatia",
            "Cuba",
            "Cyprus",
            "Czech Republic",
            "Denmark",
            "Djibouti",
            "Dominica",
            "Dominican Republic",
            "East Timor (Timor-Leste)",
            "Ecuador",
            "Egypt",
            "El Salvador",
            "Equatorial Guinea",
            "Eritrea",
            "Estonia",
            "Eswatini",
            "Ethiopia",
            "Fiji",
            "Finland",
            "France",
            "Gabon",
            "The Gambia",
            "Georgia",
            "Germany",
            "Ghana",
            "Greece",
            "Grenada",
            "Guatemala",
            "Guinea",
            "Guinea-Bissau",
            "Guyana",
            "Haiti",
            "Honduras",
            "Hungary",
            "Iceland",
            "India",
            "Indonesia",
            "Iran",
            "Iraq",
            "Ireland",
            "Israel",
            "Italy",
            "Jamaica",
            "Japan",
            "Jordan",
            "Kazakhstan",
            "Kenya",
            "Kiribati",
            "Korea, North",
            "Korea, South",
            "Kosovo",
            "Kuwait",
            "Kyrgyzstan",
            "Laos",
            "Latvia",
            "Lebanon",
            "Lesotho",
            "Liberia",
            "Libya",
            "Liechtenstein",
            "Lithuania",
            "Luxembourg",
            "Madagascar",
            "Malawi",
            "Malaysia",
            "Maldives",
            "Mali",
            "Malta",
            "Marshall Islands",
            "Mauritania",
            "Mauritius",
            "Mexico",
            "Micronesia, Federated States of",
            "Moldova",
            "Monaco",
            "Mongolia",
            "Montenegro",
            "Morocco",
            "Mozambique",
            "Myanmar (Burma)",
            "Namibia",
            "Nauru",
            "Nepal",
            "Netherlands",
            "New Zealand",
            "Nicaragua",
            "Niger",
            "Nigeria",
            "North Macedonia",
            "Norway",
            "Oman",
            "Pakistan",
            "Palau",
            "Panama",
            "Papua New Guinea",
            "Paraguay",
            "Peru",
            "Philippines",
            "Poland",
            "Portugal",
            "Qatar",
            "Romania",
            "Russia",
            "Rwanda",
            "Saint Kitts and Nevis",
            "Saint Lucia",
            "Saint Vincent and the Grenadines",
            "Samoa",
            "San Marino",
            "Sao Tome and Principe",
            "Saudi Arabia",
            "Senegal",
            "Serbia",
            "Seychelles",
            "Sierra Leone",
            "Singapore",
            "Slovakia",
            "Slovenia",
            "Solomon Islands",
            "Somalia",
            "South Africa",
            "Spain",
            "Sri Lanka",
            "Sudan",
            "Sudan, South",
            "Suriname",
            "Sweden",
            "Switzerland",
            "Syria",
            "Taiwan",
            "Tajikistan",
            "Tanzania",
            "Thailand",
            "Togo",
            "Tonga",
            "Trinidad and Tobago",
            "Tunisia",
            "Turkey",
            "Turkmenistan",
            "Tuvalu",
            "Uganda",
            "Ukraine",
            "United Arab Emirates",
            "United Kingdom",
            "United States",
            "Uruguay",
            "Uzbekistan",
            "Vanuatu",
            "Vatican City",
            "Venezuela",
            "Vietnam",
            "Yemen",
            "Zambia",
            "Zimbabwe"});
            this.CboxCountry.Location = new System.Drawing.Point(203, 205);
            this.CboxCountry.Name = "CboxCountry";
            this.CboxCountry.Size = new System.Drawing.Size(285, 23);
            this.CboxCountry.TabIndex = 16;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(106, 239);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(91, 15);
            this.label9.TabIndex = 15;
            this.label9.Text = "Phone Number:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(144, 206);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 15);
            this.label8.TabIndex = 14;
            this.label8.Text = "Country:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(139, 179);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(58, 15);
            this.label7.TabIndex = 13;
            this.label7.Text = "ZIP Code:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(68, 149);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(129, 15);
            this.label6.TabIndex = 12;
            this.label6.Text = "Region/State/Province:";
            // 
            // TboxPhone
            // 
            this.TboxPhone.Location = new System.Drawing.Point(203, 236);
            this.TboxPhone.Name = "TboxPhone";
            this.TboxPhone.Size = new System.Drawing.Size(285, 23);
            this.TboxPhone.TabIndex = 11;
            // 
            // TboxZIP
            // 
            this.TboxZIP.Location = new System.Drawing.Point(203, 176);
            this.TboxZIP.Name = "TboxZIP";
            this.TboxZIP.Size = new System.Drawing.Size(285, 23);
            this.TboxZIP.TabIndex = 9;
            // 
            // TboxRegion
            // 
            this.TboxRegion.Location = new System.Drawing.Point(203, 146);
            this.TboxRegion.Name = "TboxRegion";
            this.TboxRegion.Size = new System.Drawing.Size(285, 23);
            this.TboxRegion.TabIndex = 8;
            // 
            // TboxCity
            // 
            this.TboxCity.Location = new System.Drawing.Point(203, 117);
            this.TboxCity.Name = "TboxCity";
            this.TboxCity.Size = new System.Drawing.Size(285, 23);
            this.TboxCity.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(166, 120);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(31, 15);
            this.label5.TabIndex = 6;
            this.label5.Text = "City:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(111, 91);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(86, 15);
            this.label4.TabIndex = 5;
            this.label4.Text = "Address Line 2:";
            // 
            // TboxAddress2
            // 
            this.TboxAddress2.Location = new System.Drawing.Point(203, 88);
            this.TboxAddress2.Name = "TboxAddress2";
            this.TboxAddress2.Size = new System.Drawing.Size(285, 23);
            this.TboxAddress2.TabIndex = 4;
            // 
            // TboxAddress1
            // 
            this.TboxAddress1.Location = new System.Drawing.Point(203, 59);
            this.TboxAddress1.Name = "TboxAddress1";
            this.TboxAddress1.Size = new System.Drawing.Size(285, 23);
            this.TboxAddress1.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(111, 62);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(86, 15);
            this.label3.TabIndex = 2;
            this.label3.Text = "Address Line 1:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(133, 33);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 15);
            this.label2.TabIndex = 1;
            this.label2.Text = "Full Name:";
            // 
            // TboxFullName
            // 
            this.TboxFullName.Location = new System.Drawing.Point(203, 30);
            this.TboxFullName.Name = "TboxFullName";
            this.TboxFullName.Size = new System.Drawing.Size(285, 23);
            this.TboxFullName.TabIndex = 0;
            // 
            // GboxCreditC
            // 
            this.GboxCreditC.Controls.Add(this.TboxCVV);
            this.GboxCreditC.Controls.Add(this.label15);
            this.GboxCreditC.Controls.Add(this.CboxYear);
            this.GboxCreditC.Controls.Add(this.CboxMonth);
            this.GboxCreditC.Controls.Add(this.label14);
            this.GboxCreditC.Controls.Add(this.TboxCardNumber);
            this.GboxCreditC.Controls.Add(this.TboxCardName);
            this.GboxCreditC.Controls.Add(this.label13);
            this.GboxCreditC.Controls.Add(this.label12);
            this.GboxCreditC.Location = new System.Drawing.Point(39, 370);
            this.GboxCreditC.Name = "GboxCreditC";
            this.GboxCreditC.Size = new System.Drawing.Size(570, 151);
            this.GboxCreditC.TabIndex = 3;
            this.GboxCreditC.TabStop = false;
            this.GboxCreditC.Text = "Credit Card";
            // 
            // TboxCVV
            // 
            this.TboxCVV.Location = new System.Drawing.Point(203, 114);
            this.TboxCVV.Name = "TboxCVV";
            this.TboxCVV.Size = new System.Drawing.Size(100, 23);
            this.TboxCVV.TabIndex = 8;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(159, 117);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(38, 15);
            this.label15.TabIndex = 7;
            this.label15.Text = "CVV2:";
            // 
            // CboxYear
            // 
            this.CboxYear.DropDownHeight = 85;
            this.CboxYear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CboxYear.DropDownWidth = 57;
            this.CboxYear.FormattingEnabled = true;
            this.CboxYear.IntegralHeight = false;
            this.CboxYear.Items.AddRange(new object[] {
            "2021",
            "2022",
            "2023",
            "2024",
            "2025",
            "2026",
            "2027",
            "2028",
            "2029",
            "2030",
            "2031",
            "2032",
            "2033",
            "2034",
            "2035",
            "2036",
            "2037",
            "2038",
            "2039",
            "2040",
            "2041",
            "2042",
            "2043",
            "2044",
            "2045",
            "2046",
            "2047",
            "2048",
            "2049",
            "2050",
            "2051",
            "2052",
            "2053",
            "2054",
            "2055",
            "2056",
            "2057",
            "2058",
            "2059",
            "2060"});
            this.CboxYear.Location = new System.Drawing.Point(266, 86);
            this.CboxYear.Name = "CboxYear";
            this.CboxYear.Size = new System.Drawing.Size(57, 23);
            this.CboxYear.TabIndex = 6;
            // 
            // CboxMonth
            // 
            this.CboxMonth.DropDownHeight = 85;
            this.CboxMonth.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CboxMonth.DropDownWidth = 57;
            this.CboxMonth.FormattingEnabled = true;
            this.CboxMonth.IntegralHeight = false;
            this.CboxMonth.Items.AddRange(new object[] {
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12"});
            this.CboxMonth.Location = new System.Drawing.Point(203, 85);
            this.CboxMonth.Name = "CboxMonth";
            this.CboxMonth.Size = new System.Drawing.Size(57, 23);
            this.CboxMonth.TabIndex = 5;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(107, 89);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(90, 15);
            this.label14.TabIndex = 4;
            this.label14.Text = "Expiration Date:";
            // 
            // TboxCardNumber
            // 
            this.TboxCardNumber.Location = new System.Drawing.Point(203, 57);
            this.TboxCardNumber.Name = "TboxCardNumber";
            this.TboxCardNumber.Size = new System.Drawing.Size(285, 23);
            this.TboxCardNumber.TabIndex = 3;
            // 
            // TboxCardName
            // 
            this.TboxCardName.Location = new System.Drawing.Point(203, 28);
            this.TboxCardName.Name = "TboxCardName";
            this.TboxCardName.Size = new System.Drawing.Size(285, 23);
            this.TboxCardName.TabIndex = 2;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(110, 31);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(87, 15);
            this.label13.TabIndex = 1;
            this.label13.Text = "Name on Card:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(115, 60);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(82, 15);
            this.label12.TabIndex = 0;
            this.label12.Text = "Card Number:";
            // 
            // btnCPayment
            // 
            this.btnCPayment.Location = new System.Drawing.Point(354, 527);
            this.btnCPayment.Name = "btnCPayment";
            this.btnCPayment.Size = new System.Drawing.Size(200, 23);
            this.btnCPayment.TabIndex = 4;
            this.btnCPayment.Text = "Complete Payment";
            this.btnCPayment.UseVisualStyleBackColor = true;
            this.btnCPayment.Click += new System.EventHandler(this.btnCPayment_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(84, 527);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(200, 23);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // PaymentPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(649, 560);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnCPayment);
            this.Controls.Add(this.GboxCreditC);
            this.Controls.Add(this.GboxAddress);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.CboxPayment);
            this.Name = "PaymentPage";
            this.Text = "Payment Page";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PaymentPagecs_FormClosing);
            this.GboxAddress.ResumeLayout(false);
            this.GboxAddress.PerformLayout();
            this.GboxCreditC.ResumeLayout(false);
            this.GboxCreditC.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox CboxPayment;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox GboxAddress;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox TboxAddress2;
        private System.Windows.Forms.TextBox TboxAddress1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TboxFullName;
        private System.Windows.Forms.GroupBox GboxCreditC;
        private System.Windows.Forms.ComboBox CboxCountry;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox TboxPhone;
        private System.Windows.Forms.TextBox TboxZIP;
        private System.Windows.Forms.TextBox TboxRegion;
        private System.Windows.Forms.TextBox TboxCity;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox TboxEmail;
        private System.Windows.Forms.TextBox TboxCardNumber;
        private System.Windows.Forms.TextBox TboxCardName;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox CboxYear;
        private System.Windows.Forms.ComboBox CboxMonth;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox TboxCVV;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button btnCPayment;
        private System.Windows.Forms.Button btnCancel;
    }
}