﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace oopproje
{
    public partial class SignUp : Form
    {
        /// <summary>
        /// Code of the login form
        /// </summary>
        public SignUp()
        {
            InitializeComponent();
            textBoxPassword.PasswordChar = '●';
            textBoxPassword2.PasswordChar = '●';
            
        }
        /// <summary>
        /// Code for closing SignUp form
        /// </summary>
        /// <param name="sender">Logined customer username</param>
        /// <param name="e">Logined customer username</param>
        private void SignUp_FormClosing(object sender, FormClosingEventArgs e)
        {
            Logger logger = Logger.getInstance;
            logger.addLog("SignUp_FormClosing");

            Environment.Exit(0);
        }
        /// <summary>
        /// button for going back to login page
        /// </summary>
        /// <param name="sender">Logined customer username</param>
        /// <param name="e">Logined customer username</param>
        private void btnBack_Click(object sender, EventArgs e)
        {
            Logger logger = Logger.getInstance;
            logger.addLog("btnBack_Click");

            this.Hide();
            Login login = new Login();
            login.Show();
        }
        /// <summary>
        /// Checks email validation
        /// </summary>
        /// <param name="sender">Logined customer username</param>
        /// <param name="e">Logined customer username</param>
        bool IsValidEmail(string email)
        {
            Logger logger = Logger.getInstance;
            logger.addLog("IsValidEmail");

            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }
        /// <summary>
        /// Sends user input to database for registration
        /// </summary>
        /// <param name="sender">Logined customer username</param>
        /// <param name="e">Logined customer username</param>
        private void btnSignUpConfirm_Click(object sender, EventArgs e)
        {
            Logger logger = Logger.getInstance;
            logger.addLog("btnSignUpConfirm_Click");

            if (!(textBoxPassword.Text == textBoxPassword2.Text))
            {
                MessageBox.Show("Password are not matching!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (!IsValidEmail(textBoxEmail.Text))
            {
                MessageBox.Show("Email is not valid!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                Customer customer = new Customer(textBoxName.Text,textBoxSurname.Text,textBoxAdress.Text,textBoxEmail.Text,textBoxUsername.Text,textBoxPassword.Text);
                customer.saveCustomer();
                this.Hide();
                Login login = new Login();
                login.Show();
             
            }          
        }
    }
}
